# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

from bottle import *
from bottle_websocket import GeventWebSocketServer
from bottle_websocket import websocket
from geventwebsocket import WebSocketError
import json
from module_loader import ModuleLoader
import websocket_func
import argparse
import os
import logging
from logging.handlers import RotatingFileHandler

VERSION = 0.2

@route('/')
def index(section='home'):
    return template('index')

@route('/dashboard.html')
def angular():
    return template('dashboard')

@route('/hostpage.html')
def angular():
    return template('hostpage')

@route('/notifications.html')
def angular():
    return template('notifications')

@route('/scan.html')
def angular():
    return template('scan')

@route('/settings.html')
def angular():
    return template('settings')

@route('/signin.html')
def angular():
    return template('signin')

@route('/stats.html')
def angular():
    return template('stats')

@route('/users.html')
def angular():
    return template('users')

#  To get static files
@get('/static/<filepath:path>')
def static(filepath):
    return static_file(filepath, root='static')


#  To get Bower dependancies
@get('/bower_components/<filepath:path>')
def bower_files(filepath):
    return static_file(filepath, root='bower_components')


#  Creation of a websocket for communicating with a client
@get('/websocket', apply=[websocket])
def receive(ws):
    global ml
    ml.get_websocket_container().add_websocket(ws)
    logger = logging.getLogger("mum_log")
    while True:
        try:
            response = ws.receive()
            if response is not None:
                msg = json.loads(response)
                for code in msg:
                    try:
                        getattr(websocket_func, code)(msg, ws, ml)
                    except AttributeError:
                        logger.error("Unknown websocket code: " + code)
        except WebSocketError:   # Should be WebSocketError when closing the connection
            ml.get_websocket_container().remove_websocket(ws)
            break

@route('/upload', method='POST')
def do_upload():
    upload = request.files.get('upload')
    try:
        upload.save(ml.get_public_keys_loc() + upload.filename)
        return 'Upload done! <a href="/">Back to dashboard.</a>'
    except IOError:
        return 'This file already exists. <a href="/">Back to dashboard.</a>'

@error(500)
def handle_500_error(error_obj):
    logger = logging.getLogger("mum_log")
    logger.error("Error 500.")
    logger.error(error_obj.body)
    logger.error(error_obj.traceback)
    return "Error 500 "+ error_obj.body + " (see the log for traceback)"

def create_logger(dict_conf):
    logger = logging.getLogger("mum_log")
    level = getattr(logging, dict_conf["log_level"])
    logger.setLevel(level)
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    file_handler = RotatingFileHandler(dict_conf['log_location'], 'a', 1000000, 1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)

    return logger

#  Launching the server
if __name__ == '__main__':
    global ml

    # creating the parser for the command line arguments
    parser = argparse.ArgumentParser(description="Mum (Machines Under Monotoring) is a web-oriented and modulable "
                                                 "monitoring service.",
                                     epilog="The following fields must be specified on the configuration file or by "
                                            "arguments in order to launch the service: server_port, server_addr, "
                                            "db_location.")
    parser.add_argument("--conf", help="the location of the configuration file (can be relative from the launcher)")
    parser.add_argument("--server_port", type=int, help="the port of the service.")
    parser.add_argument("--server_addr", help="the address of the service.")
    parser.add_argument("--db_location", help="the location of the shelve database.")
    parser.add_argument("--external_modules_location", help="the location of the external monitoring modules.")
    parser.add_argument("--keys_location", help="the location of the directory containing the SSH keys.")
    parser.add_argument("--smtp_server", help="name of the SMTP server to send e-mail notifications")
    parser.add_argument("--smtp_port", help="port number of the SMTP server")
    parser.add_argument("--smtp_address", help="e-mail address of the sender for e-mail notifications")
    parser.add_argument("--log_level", help="set the log level : DEBUG, INFO, WARNING, ERROR")
    parser.add_argument("--scan_command", help="scan command to use (default is sudo nmap)")
    args = parser.parse_args()

    # creating the default conf structure from the configuration file
    conf = {'scan': []}

    files_to_read = []

    if os.path.isfile(args.conf):
        # the argument is a file name
        files_to_read.append(args.conf)
    else:
        # the argument is a directory name
        path = args.conf + '/'
        for dir_content in os.listdir(args.conf):
            files_to_read.append(str(path + dir_content))
        # sorting the list in lexicographic order
        files_to_read = sorted(files_to_read, key=str.lower)

    found_conf_file = False
    for file_name in files_to_read:
        # we read only files with '.conf' extension
        if re.search('^.+\.conf$', file_name):
            found_conf_file = True
            fconf = open(file_name, 'r')
            for line in fconf.read().splitlines():
                if len(line) > 0 and line[0] != '#':
                    fields = line.split('=')
                    if fields[0] == 'scan':
                        scan_fields = fields[1].split('|')
                        dict_scan = {'scan_name': scan_fields[0]}
                        dict_scan['scan_description'] = scan_fields[1]
                        dict_scan['scan_priority'] = int(scan_fields[2])
                        dict_scan['scan_options'] = scan_fields[3]
                        conf['scan'].append(dict_scan)
                    else:
                        conf[fields[0]] = fields[1]
            fconf.close()
    if not found_conf_file:
        p = argparse.ArgumentParser()
        p.exit(status=0, message="No configuration file found in the path given. Does at least one '.conf' file exists?\n")

    # now, we overwrite each field of the configuration which are specified by command line argument
    dict_args = vars(args)
    for arg in dict_args:
        if dict_args[arg] is not None:
            conf[arg] = dict_args[arg]

    logger = create_logger(conf)

    ml = ModuleLoader(conf, VERSION)
    ml.load_all_monitoring_modules()
    ml.load_all_connection_modules()
    ml.load_all_detection_modules()
    ml.load_all_notification_modules()
    ml.get_db().init_global_conf(ml.get_info_mod_monitoring())
    ml.start_monitoring()
    #dict_notif = ml.db.add_check('127.0.0.1', "ping", False)
    #ml.run_notification_modules(dict_notif)
    port = int(os.environ.get('PORT', int(conf['server_port'])))
    run(host=conf['server_addr'], port=port, debug=True, server=GeventWebSocketServer)
    # after ending
    ml.stop_monitoring()
