# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import threading
from datetime import datetime
from datetime import timedelta
import time

waiting_list = []
end = False


def init(ml,wsc):
    for instr in ml.get_all_monitoring_instructions():
        add_to_waiting_list(instr)
    pm = ProcessMonitoring(ml, wsc)
    pm.start()


class ProcessMonitoring(threading.Thread):
    """
    Runs the monitoring modules at the specified time.
    Attributes : - waiting_list : a list containing structured data concerning the monitring modules to launch
    in the form
    {
        'addr' : val,       => the IP address of the host
        'mod_name', val,    => the name of the monitoring module
        'time', val,        => the time at when to launch the monitoring module
        'freq', val         => the frequency check (in seconds)
    }
    The data are ordored by crescent time. This is for optimize the complexity when poping the most recent time.
     We could use here the deque structure from Python in order to use optimized poping left, but this structure
     don't have implemented the insert() function, which is used for adding new data and keep the list ordored.
    """
    def __init__(self, ml, wsc):
        threading.Thread.__init__(self)
        self.ml = ml
        self.wsc = wsc

    def run(self):
        """
        Each second, the process will check if the last element of the list have to be lauch.
        If it is the case, it will add this element to the launching list, as well with the previous elements
        having the same launching date.
        """
        global end
        global waiting_list
        ready_to_launch = False
        rm = None
        while not end:
            if not waiting_list == []:
                modules_to_run = []
                while waiting_list[len(waiting_list) - 1]['time'] <= datetime.now():
                    dict_mod = waiting_list.pop(len(waiting_list) - 1)
                    modules_to_run.append(dict_mod)
                    dict_mod['time'] = datetime.now() + timedelta(seconds=int(dict_mod['freq']))
                    add_to_waiting_list(dict_mod)
                    ready_to_launch = True
                if ready_to_launch:
                    rm = RunMonitoring(modules_to_run, self.ml, self.wsc)
                    rm.start()
                    ready_to_launch = False
            time.sleep(1)
        if rm is not None:
            rm.join(5)


def add_to_waiting_list(dict_mod):
    """
    Adds an element to the waiting list and keep it ordered by crescent launching times.
    :param dict_mod: a dictionary containing:
    {'addr': str, 'mod_name': str, 'time': datetime, 'freq': int}
    """
    global waiting_list
    if waiting_list == []:
        waiting_list.append(dict_mod)
    else:
        pos_list = 0
        while pos_list < len(waiting_list) and dict_mod['time'] <= waiting_list[pos_list]['time']:
            pos_list += 1
        waiting_list.insert(pos_list, dict_mod)


def remove_to_waiting_list(addr_host, modname):
    """
    Removes from the waiting list all occurrences of the given host for the module modname.
    If modname is None, all entries for the host are removed.
    :param addr_host: the IP address of the host
    :param modname: the name of the monitoring module.
    """
    global waiting_list
    i = 0
    while i < len(waiting_list):
        if waiting_list[i]['addr'] == addr_host:
            if modname is None:
                # it was asked to remove all entries of this host
                waiting_list.pop(i)
                i -= 1
            elif waiting_list[i]['mod_name'] == modname:
                # we remove only the necessary module from the list
                waiting_list.pop(i)
                i -= 1
        i += 1


def update_mod_on_waiting_list(new_conf):
    """
    Updates the check frequency of a module after its configuration changed. The next check will be launched right after
    this new configuration.
    :param new_conf: a structure containing the values :
    {
        'addr_host': val,
        'mod_name': val,
        'freq' : val,
        'minor_limit' : val,
        'major_limit' : val
    }
    """
    global waiting_list
    for i in range(len(waiting_list)):
        if waiting_list[i]['addr'] == new_conf['addr_host'] and waiting_list[i]['mod_name'] == new_conf['mod_name']:
            prev_conf = waiting_list.pop(i)
            prev_conf['freq'] = new_conf['freq']
            prev_conf['time'] = datetime.now()
            add_to_waiting_list(prev_conf)


class RunMonitoring(threading.Thread):
    def __init__(self, list_dict_mod, ml, wsc):
        threading.Thread.__init__(self)
        self.list_dict_mod = list_dict_mod
        self.ml = ml
        self.wsc = wsc

    def run(self):
        #sys.stdout.flush()
        for dict_mod in self.list_dict_mod:
            #sys.stdout.flush()
            #print "Launching " + str(dict_mod['mod_name']) + " on " + str(dict_mod['addr'])
            #sys.stdout.flush()
            self.ml.run_one_monitoring_module(dict_mod['mod_name'], dict_mod['addr'])
        self.wsc.notify_state_change()