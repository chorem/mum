# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"


__author__ = 'aguilbaud'
import json
import threading
import logging
from geventwebsocket import WebSocketError

#  ---- Definitions for the nmap detection procedure ----

# To Launch the nmap detection with a new thread
class ThreadDetect(threading.Thread):
    def __init__(self, param, opt, ml, ws):
        threading.Thread.__init__(self)
        self.param = param  #  ip_range or hostname
        self.opt = opt      #  nmap options
        self.ml = ml        #  module_loader
        self.ws = ws        #  websocket connection

    def run(self):
        task_name = "Scanning " + self.param
        self.ml.create_task(task_name)
        scanned_ip = self.ml.run_nmap_detection(self.param, self.opt, self.ws)
        if scanned_ip is not None:
            try:
                self.ws.send(json.dumps({"SUCCESS_MODULE": scanned_ip}))
            except WebSocketError:
                pass
            for ip in json.loads(scanned_ip):
                self.ml.run_all_detection_modules(ip)
                monitoring_intructions = self.ml.get_db().get_monitoring_instructions(ip)
                for instr in monitoring_intructions:
                    self.ml.add_to_waiting_list(instr)
        self.ml.remove_task(task_name)


#  Launching of the detection after getting a ip range.
def start_first_detection(args, ml, ws):
    t = ThreadDetect(args['ip_range'], args['nmap_options'], ml, ws)
    t.start()


#  ---- Functions corresponding to a websocket code ----

# asked from scan page
def NMAP_SCAN_DEMAND(msg, ws, ml):
    logger = logging.getLogger("mum_log")
    if msg["NMAP_SCAN_DEMAND"]["nmap_options"] == '':
        ml.create_empty_host(msg["NMAP_SCAN_DEMAND"]["ip_range"], ws)
        ws.send(json.dumps({"SUCCESS_MODULE": "New host successfully created"}))
    else:
        start_first_detection(msg["NMAP_SCAN_DEMAND"], ml, ws)


# asked from hostpage
def LAUNCH_FULL_DETECTION(msg, ws, ml):
    suceess_detection = ml.run_all_detection_modules(msg["LAUNCH_FULL_DETECTION"])
    if suceess_detection:
        ws.send(json.dumps({"SUCCESS_MODULE": "Full detection"}))
    else:
        ws.send(json.dumps({"ERROR": "Full detection has failed (is SSH well configured?)"}))


# asked from head controller
def GET_HOSTS(msg, ws, ml):
    db = ml.get_db()
    ws.send(json.dumps({"RES_GET_HOSTS": db.get_hosts()}))


# asked from hostpage
def GET_HOST_INFO(msg, ws, ml):
    dict_host_info = ml.get_host_info(msg["GET_HOST_INFO"])
    dict_host_info['loaded_moni_mod'] = ml.get_info_mod_monitoring()
    ws.send(json.dumps({"RES_INFO_HOST": dict_host_info}))


# asked from hostpage
def REM_HOST(msg, ws, ml):
    ml.remove_host(msg["REM_HOST"])
    ws.send(json.dumps({"RES_REM_HOST": True}))


# asked when the request can directly pass by database
def CALL_FUNC_DB(msg, ws, ml):
    res = ml.launch_db_function(msg["CALL_FUNC_DB"])
    msg = json.dumps({"RES_CALL_FUNC_DB": {
        "func": msg["CALL_FUNC_DB"]['func'],
        "res": res
    }})
    ws.send(msg)

# asked from dashboard, on category view
def GET_LOADED_MONI_MOD(msg, ws, ml):
    ws.send(json.dumps({"RES_GET_LOADED_MONI_MOD": ml.get_info_mod_monitoring()}))

# asked from hostpage, at the connection configuration
def GET_LOADED_CONN_MOD(msg, ws, ml):
    ws.send(json.dumps({"RES_GET_LOADED_CONN_MOD": ml.get_info_mod_conn()}))


# asekd from dashboard
def GET_LOADED_NOTIF_MOD(msg, ws, ml):
    ws.send(json.dumps({"RES_GET_LOADED_NOTIF_MOD": ml.get_info_mod_notification()}))


# asked from hostpage
def SET_IDLE_STATE(msg, ws, ml):
    ml.set_idling_state(msg["SET_IDLE_STATE"])
    dict_host_info = ml.get_host_info(msg["SET_IDLE_STATE"])
    dict_host_info['loaded_moni_mod'] = ml.get_info_mod_monitoring()
    ws.send(json.dumps({"RES_INFO_HOST": dict_host_info}))
    ws.send(json.dumps({"RES_GET_HOSTS": ml.get_db().get_hosts()}))


# asked from hostpage
def SET_MOD_ACTIVATION(msg, ws, ml):
    ml.update_activated_modules(msg["SET_MOD_ACTIVATION"])
    ws.send(json.dumps({"RES_GET_HOSTS": ml.db.get_hosts()}))


# asked from hostpage, at the connection configuration modal
def TEST_CONN(msg, ws, ml):
    try:
        ml.test_connection(msg["TEST_CONN"]["addr_host"],
                           msg["TEST_CONN"]["conn_mod_name"])
        ws.send(json.dumps({"RES_TEST_CONN": True}))
    except Exception:
        ws.send(json.dumps({"RES_TEST_CONN": False}))


# asked from hostpage, at the monitoring module configuration
def SET_CONF_MOD(msg, ws, ml):
    ml.set_conf_moni_mod(msg["SET_CONF_MOD"])


# asekd from hostpage
def CHECK_NOW(msg, ws, ml):
    args = msg["CHECK_NOW"]
    ml.run_one_monitoring_module(args['mod_name'], args['addr_host'])
    ws.send(json.dumps({"RES_INFO_HOST": ml.get_host_info(args['addr_host'])}))


# asked from hostpage, at the connection configuration
def GET_KEYS_LIST(msg, ws, ml):
    ws.send(json.dumps({"KEYS_LIST": ml.get_public_keys_list()}))


# asked from the header controller
def TASK_LIST(msg, ws, ml):
    ws.send(json.dumps({"RES_TASK_LIST": ml.get_db().get_task_list()}))


def GET_SCAN_OPTIONS(msg, ws, ml):
    ws.send(json.dumps({"RES_GET_SCAN_OPTIONS": ml.get_scan_options()}))


def GET_LOGS(msg, ws, ml):
    log_size = msg['GET_LOGS']
    ws.send(json.dumps({'RES_GET_LOGS': ml.get_logs(log_size)}))


def GET_ALL_MODULES_LIST(msg, ws, ml):
    res = {}
    res['monitoring'] = []
    res['connection'] = []
    res['detection'] = []
    res['notification'] = []
    for moni_mod in ml.get_info_mod_monitoring():
        res['monitoring'].append(moni_mod)
    for conn_mod in ml.get_conection_modules_list():
        res['connection'].append(conn_mod)
    for detect_mod in ml.get_loaded_detection_modules():
        res['detection'].append(detect_mod)
    for notif_mod in ml.get_info_mod_notification():
        res['notification'].append(notif_mod)
    ws.send(json.dumps({'RES_GET_ALL_MODULES_LIST': res}))