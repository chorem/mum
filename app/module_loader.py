# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import modules
import modules.connection_modules
import modules.detection_modules
import modules.monitoring_modules
import modules.storage_modules
import modules.ModuleNotCompatibleException
import modules.CommandNotFoundException
import modules.HostNotFoundException
from modules.notification_modules.websocket_container import WebSocketContainer
import process_monitoring

import json
import re
import pkgutil
import sys
import os
import traceback
import logging
import pexpect


class ModuleLoader:
    """
    Loads dynamically modules from packages connection_modules, detection_modules, monitoring_modules, storage_modules
    and contains several methods in order to call the methods once these modules loaded.
    """
    def __init__(self, conf, mum_vesion):
        self.logger = logging.getLogger("mum_log")
        self.conf = conf
        self.db = self.load_db()
        db_version = self.db.get_db_version()
        #if float(db_version) < mum_vesion:
        #    self.migrate_db(db_version)
        self.db.reset_tasks()
        self.loaded_mod_moni = {}  # See load_all_monitoring_modules
        self.loaded_mod_detect = {}  # See load_all_detection_modules
        self.loaded_mod_conn = {}  # See load_all_connection_modules
        self.loaded_mod_notif = {}  # See load_all_notification_modules
        self.compatible_os_list = ['other'] # Will contain the list of os compatibles for every monitoring module loaded
        self.wsc = WebSocketContainer(self.db)

    def load_db(self):
        """
        Creates an instance of the class shelve_db from storage_modules.
        :return: an instance of the shelve_db class
        """
        db_name = "shelve_db"
        db = __import__("modules.storage_modules." + db_name, fromlist=modules.storage_modules)
        db_instance = getattr(db, db_name)(self.conf['db_location'])
        return db_instance

    def get_db(self):
        return self.db

    def migrate_db(self, current_db_version):
        """
        Migrates the database through the rules defined on migration_rules.json
        A backup of the previous database is also done.
        """
        # backup creation
        db_loc_tree = self.conf['db_location'].split('/')
        old_db_name = db_loc_tree[len(db_loc_tree) -1]
        db_loc_tree.pop(len(db_loc_tree) -1)
        backup_location = self.conf['db_location'] + '_v_' + str(current_db_version) + '.back'
        pexpect.run("cp " + self.conf['db_location'] + " " + backup_location)

        migration_rules_file = open("migration_rules.json")
        migration_rules = json.loads(migration_rules_file.read())
        for version in migration_rules:
            if float(version) > current_db_version:
                for new_val in migration_rules[version]["add_field"]:
                    self.db.add_field(new_val, migration_rules[version]["add_field"][new_val])
                for val_to_rem in migration_rules[version]["rem_field"]:
                    self.db.rem_field(val_to_rem, migration_rules[version]["rem_field"][val_to_rem])
            #self.db.set_db_version(version)
        return None

    def get_public_keys_loc(self):
        return self.conf['keys_location']

    def get_websocket_container(self):
        return self.wsc

    def get_scan_options(self):
        return self.conf['scan']

    """
    def get_all_known_ports(self):
        res = []
        for conn_mod in self.loaded_mod_conn:
            res.append(self.loaded_mod_conn[conn_mod]['known_port'])
        return res
    """

    def create_task(self, task_id):
        self.db.store_task(task_id)
        self.wsc.notify_task_change()

    def remove_task(self, task_id):
        self.db.remove_task(task_id)
        self.wsc.notify_task_change()

    @staticmethod
    def add_to_waiting_list(instr):
        process_monitoring.add_to_waiting_list(instr)

    def start_monitoring(self):
        process_monitoring.init(self, self.wsc)

    @staticmethod
    def stop_monitoring():
        process_monitoring.end = True

    def run_nmap_detection(self, param, opt, ws):
        """
        Instanciates the nmap_detection module from detection_modules, and runs the detection.
        :param param: parameter to put in nmap command. can be a hostname, a ip address or a ip range
        :param opt: a string containing the options specified for the nmap command
        :param ws: a websocket connection
        :return: a list containing the IP adresses checked
        """

        nmap_mod = __import__("modules.nmap_detection", fromlist=modules)
        nmap_mod_instance = getattr(nmap_mod, "nmap_detection")(opt,
                                                                self.db,
                                                                ws,
                                                                self.get_conection_modules_list(),
                                                                self.get_monitoring_modules_list(),
                                                                self.conf['scan_command'],
                                                                modules.HostNotFoundException)
        try:
            if re.search('^\d{1,3}(-\d{1,3})?[.]\d{1,3}(-\d{1,3})?[.]\d{1,3}(-\d{1,3})?[.]\d{1,3}(-\d{1,3})?$', param):
                # the parameter is an IP range
                ip_range = nmap_mod_instance.check_ip_range(param)
            else:
                # the parameter is not an IP range, so we considere it as an hostname.
                ip_range = nmap_mod_instance.launch_detection_with_hostname(param)
            for ip in json.loads(ip_range):
                #remonving all monitoring instructions for this ip, if rescan
                process_monitoring.remove_to_waiting_list(ip, None)
            return ip_range
        except modules.HostNotFoundException.HostNotFoundException as hnfe:
            self.logger.warning(hnfe.__str__())
            ws.send(json.dumps({"ERROR": hnfe.__str__()}))
            return None

    def create_empty_host(self, addr_host, ws):
        """
        If for some reason the nmap scan is impossible, it is possible to add a host without preliminary nmap scan.
        Therefore, a fake empty nmap result is generated and the host is added this way on the database.
        :param addr_host: a string containing the IP address of the host.
        """
        if re.search('^\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}$', addr_host):
            fake_nmap_res = {}
            fake_nmap_res['os'] = 'unknown'
            fake_nmap_res['addr'] = addr_host
            fake_nmap_res['hostname'] = ''
            fake_nmap_res['openports'] = []
            self.db.add_host(addr_host,
                             json.dumps(fake_nmap_res),
                             self.get_conection_modules_list(),
                             self.get_monitoring_modules_list())
            monitoring_intructions = self.db.get_monitoring_instructions(addr_host)
            for instr in monitoring_intructions:
                self.add_to_waiting_list(instr)
        else:
            ws.send(json.dumps({"ERROR": "You must specify a valid IP address to add a host without scan"}))

    def create_connection(self, addr_host, conf_conn):
        """
        Creates a connection with a host, from the configured ones.
        :param addr_host: the IP address of the host we want to establish a connection.
        :param conf_conn: a structure containing informations about the connection to instanciate
        :return: An instance of the connection module.
        """
        conn_inst = getattr(self.loaded_mod_conn[conf_conn['conn_mod_name']]['imported'],
                            self.loaded_mod_conn[conf_conn['conn_mod_name']]['class_name'])(addr_host,
                                                                                      conf_conn['args'],
                                                                                      self.conf['keys_location'],
                                                                                      modules.CommandNotFoundException)
        return conn_inst

    def test_connection(self, addr_host, conn_mod_name):
        """
        Try to instanciate a connection module, with the address of the host given and the actual configuration for
        this connection.
        :param addr_host: The IP address of the host
        :param conn_mod_name: The name of the connection module to instanciate.
        """
        avaliable_conn = self.db.get_conf_conn(addr_host)
        conn = None
        self.logger.debug(str(avaliable_conn))
        i = 0
        conn_found = False
        while not conn_found and i < range(len(avaliable_conn)):
            if avaliable_conn[i]['conn_mod_name'] == conn_mod_name:
                conn_found = True
                conn = avaliable_conn.pop(i)
            i += 1
        getattr(self.loaded_mod_conn[conn['conn_mod_name']]['imported'],
                           self.loaded_mod_conn[conn['conn_mod_name']]['class_name'])(addr_host,
                                                                                      conn['args'],
                                                                                      self.conf['keys_location'],
                                                                                      modules.CommandNotFoundException)

    def load_all_detection_modules(self):
        """
        Instanciates and stores the informations about each monitoring modules avaliable on the loaded_mod_detect attribute
        """
        for importer, mod_name, ispkg in pkgutil.iter_modules(["app/modules/detection_modules/"]):
            if mod_name not in sys.modules:
                try:
                    loaded_mod = __import__("modules.detection_modules." + mod_name, fromlist=[mod_name])
                    infos_mod = {}
                    infos_mod['imported'] = loaded_mod
                    infos_mod['compatible_conn'] = getattr(loaded_mod, 'connection')
                    infos_mod['compatible_os'] = getattr(loaded_mod, 'compatible_os')
                    for os in infos_mod['compatible_os']:
                        if os not in self.compatible_os_list:
                            self.compatible_os_list.append(os)
                    self.loaded_mod_detect[mod_name] = infos_mod
                except AttributeError:
                    self.logger.warning("Error : internal detection module " + mod_name + " could not have been loaded.")
                    self.logger.debug(traceback.format_exc())

    def get_loaded_detection_modules(self):
        return self.loaded_mod_detect

    def run_all_detection_modules(self, addr_host):
        """
        Instanciates and runs the run_detection() method from each detection modules loaded on loaded_mod_detect
        :param addr_host: the IP address of the host we want to run the detection
        """
        conf_conn = self.db.get_conf_conn(addr_host)
        success_detection = False
        for mod_name in self.loaded_mod_detect:
            for i in range(len(conf_conn)):
                if self.loaded_mod_detect[mod_name]['compatible_conn'] == conf_conn[i]['conn_mod_name']:
                    try:
                        conn_inst = self.create_connection(addr_host, conf_conn[i])
                        res_detection = getattr(self.loaded_mod_detect[mod_name]['imported'],
                                                'run_detection')(conn_inst)
                        self.db.save_detection(conn_inst.get_addr_host(),
                                               getattr(self.loaded_mod_detect[mod_name]['imported'], 'part'),
                                               json.dumps(res_detection)
                                               )
                        success_detection = True
                    except modules.ModuleNotCompatibleException.ModuleNotCompatibleException as mnce:
                        self.logger.warning(mnce.__str__())
                    except modules.CommandNotFoundException.CommandNotFoundException as cnfe:
                        self.logger.warning(cnfe.__str__())
                    except Exception:
                        self.logger.error("An unexpected error occured during the full detection of " + addr_host)
                        self.logger.debug(traceback.format_exc())
        return success_detection

    def load_all_monitoring_modules(self):
        """
        Instanciates and stores the informations about each monitoring modules avaliable (internal and loaded
        externally) on the loaded_mod_moni attribute
        """
        for importer, mod_name, ispkg in pkgutil.iter_modules(["app/modules/monitoring_modules/"]):
            if mod_name not in sys.modules:
                try:
                    loaded_mod = __import__("modules.monitoring_modules." + mod_name, fromlist=[mod_name])
                    part = getattr(loaded_mod, 'part')
                    mod_pretty_name = getattr(loaded_mod, 'name')
                    if part not in self.loaded_mod_moni:
                        self.loaded_mod_moni[part] = {}
                        self.loaded_mod_moni[part]['compatible_os'] = []
                        self.loaded_mod_moni[part]['compatible_conn'] = []
                        self.loaded_mod_moni[part]['modules'] = {}
                    if mod_pretty_name not in self.loaded_mod_moni[part]['modules']:
                        self.loaded_mod_moni[part]['modules'][mod_pretty_name] = {}
                    self.loaded_mod_moni[part]['modules'][mod_pretty_name]['imported'] = loaded_mod
                    self.loaded_mod_moni[part]['modules'][mod_pretty_name]['external'] = False
                    self.loaded_mod_moni[part]['compatible_os'] = \
                        list(set(self.loaded_mod_moni[part]['compatible_os'] + getattr(loaded_mod, 'compatible_os')))
                    self.loaded_mod_moni[part]['compatible_conn'].append(getattr(loaded_mod, 'connection'))
                    self.loaded_mod_moni[part]['compatible_conn'] = \
                        list(set(self.loaded_mod_moni[part]['compatible_conn']))
                    try:
                        self.loaded_mod_moni[part]['param'] = getattr(loaded_mod, 'param')
                    except AttributeError:
                        self.loaded_mod_moni[part]['param'] = {}
                    try:
                        self.loaded_mod_moni[part]['default'] = getattr(loaded_mod, 'default')
                    except AttributeError:
                        self.loaded_mod_moni[part]['default'] = {}
                except AttributeError:
                    self.logger.warning("Error : internal monitoring module " + mod_name + " could not have been loaded.")
                    self.logger.debug(traceback.format_exc())

        # Now for external modules:
        if self.conf['external_modules_location'] is not None:
            sys.path.insert(0, self.conf['external_modules_location'])  # Adding the external diretory into pythonpath
            for importer, mod_name, ispkg in pkgutil.iter_modules([self.conf['external_modules_location']]):
                if mod_name not in sys.modules:
                    loaded_mod = __import__(mod_name, fromlist=[mod_name])
                    part = getattr(loaded_mod, 'part')
                    mod_pretty_name = getattr(loaded_mod, 'name')
                    if part in self.loaded_mod_moni and mod_pretty_name in self.loaded_mod_moni[part]['modules']:  # if the module overrides an internal one
                        for attr in dir(loaded_mod):  # for each attribute on the external module
                            if not re.search('^_{2}\S*_{2}$', attr):  # we override each declarated attribute
                                                                        # (we don't override those named __*__)
                                setattr(self.loaded_mod_moni[part]['modules'][mod_pretty_name]['imported'],
                                        attr,
                                        getattr(loaded_mod, attr))
                    else:  # otherwise, load the external module normally
                        try:
                            if part not in self.loaded_mod_moni:
                                self.loaded_mod_moni[part] = {}
                                self.loaded_mod_moni[part]['compatible_os'] = []
                                self.loaded_mod_moni[part]['compatible_conn'] = []
                                self.loaded_mod_moni[part]['modules'] = {}
                            if mod_pretty_name not in self.loaded_mod_moni[part]['modules']:
                                self.loaded_mod_moni[part]['modules'][mod_pretty_name] = {}
                            self.loaded_mod_moni[part]['modules'][mod_pretty_name]['imported'] = loaded_mod
                            self.loaded_mod_moni[part]['modules'][mod_pretty_name]['external'] = True
                            self.loaded_mod_moni[part]['compatible_os'] = \
                                list(set(self.loaded_mod_moni[part]['compatible_os'] + getattr(loaded_mod, 'compatible_os')))
                            self.loaded_mod_moni[part]['compatible_conn'].append(getattr(loaded_mod, 'connection'))
                            self.loaded_mod_moni[part]['compatible_conn'] = \
                                list(set(self.loaded_mod_moni[part]['compatible_conn']))
                            try:
                                self.loaded_mod_moni[part]['param'] = getattr(loaded_mod, 'param')
                            except AttributeError:
                                self.loaded_mod_moni[part]['param'] = {}
                            try:
                                self.loaded_mod_moni[part]['default'] = getattr(loaded_mod, 'default')
                            except AttributeError:
                                self.loaded_mod_moni[part]['default'] = {}
                        except AttributeError:
                            self.logger.warning("Error : external monitoring module " + mod_name + " could not have been loaded.")
                            self.logger.debug(traceback.format_exc())

    def run_one_monitoring_module(self, part_name, addr_host):
        """
        This function will first ask for the instanciation of the most prioritary connection. Then, for each module that
        can check the requested part, we will see if the instanciated connection can be use for the check. If so, it
        will run the 'check' function of the corresponding monitoring module with this connection.
        :param part_name: the name of the part to check
        :param addr_host: the IP address of the host
        """
        try:
            if part_name in self.loaded_mod_moni[part_name]['modules'] and \
                    getattr(self.loaded_mod_moni[part_name]['modules'][part_name]['imported'], 'connection') == "":
                # the module which check this part doesn't need a connection
                port_list = self.db.get_nmap_result(addr_host)['openports']
                try:
                    res_check = getattr(self.loaded_mod_moni[part_name]['modules'][part_name]['imported'],
                                        'check')(addr_host,
                                                 port_list,
                                                 modules.CommandNotFoundException)
                    dict_notif = self.db.add_check(addr_host, part_name, res_check)
                    self.run_notification_modules(dict_notif)
                except modules.CommandNotFoundException.CommandNotFoundException:
                    msg = "No port " + part_name + " on " + addr_host + ". This part checking have been deactivated."
                    self.logger.warning(msg)
                    self.wsc.notify_module_deactivation(msg)
                    process_monitoring.remove_to_waiting_list(addr_host, part_name)
                    dict_deactivation_request = {}
                    dict_deactivation_request['addr_host'] = addr_host
                    dict_deactivation_request['activated'] = {part_name: False}
                    self.db.config_mod_activation(dict_deactivation_request)
            else:
                # to check this part, it is necessary to create a connection with the host...
                # getting all availiable connections for this part to check
                conf_conn = self.db.get_conf_conn(addr_host)
                check_done = False
                i = 0
                while i < len(conf_conn) and not check_done:
                    # for each connection that have been configurated for this host (by priority)
                    for mod in self.loaded_mod_moni[part_name]['modules']:
                        # for each monitoring module, being from this part, activated for this host
                        loaded_mod = self.loaded_mod_moni[part_name]['modules'][mod]['imported']
                        if getattr(loaded_mod, 'connection') == conf_conn[i]['conn_mod_name']:
                            subpart_list = self.db.get_subpart(addr_host, part_name)
                            # if this monitoring module is compatible for the current connection
                            try:
                                conn_inst = self.create_connection(addr_host, conf_conn[i])
                                res_check = getattr(loaded_mod,
                                                    'check')(conn_inst,
                                                             modules.ModuleNotCompatibleException,
                                                             subpart_list)
                                dict_notif = self.db.add_check(conn_inst.get_addr_host(), part_name, res_check)
                                self.run_notification_modules(dict_notif)
                                check_done = True
                            except Exception:
                                self.logger.warning("The connection could not have been established with " + conf_conn[i]['conn_mod_name']\
                                      + " on " + addr_host)
                                self.logger.debug(traceback.format_exc())
                                self.logger.warning("Now trying on next avaliable connection...")
                    i += 1
                if not check_done:
                    msg = "No necessary connection have been properly configured for " + part_name + " on " + addr_host + \
                          ". Therefore it has been deactivated."
                    self.logger.warning(msg)
                    self.wsc.notify_module_deactivation(msg)
                    process_monitoring.remove_to_waiting_list(addr_host, part_name)
                    dict_deactivation_request = {}
                    dict_deactivation_request['addr_host'] = addr_host
                    dict_deactivation_request['activated'] = {part_name: False}
                    self.db.config_mod_activation(dict_deactivation_request)
        except KeyError:
            msg = "The module  " + part_name + " has not been loaded. Desactivation for " + addr_host
            self.logger.warning(msg)
            self.wsc.notify_module_deactivation(msg)
            process_monitoring.remove_to_waiting_list(addr_host, part_name)
            dict_deactivation_request = {}
            dict_deactivation_request['addr_host'] = addr_host
            dict_deactivation_request['activated'] = {part_name: False}
            self.db.config_mod_activation(dict_deactivation_request)

    def get_monitoring_modules_list(self):
        """
        Get information about the output, block, compatible os and class name of the monitoring modules.
        These informations must be specified on each modules, and must be loaded at the launch of the application.
        :return: a dictionary containing these informations on the form :
        {'min_15_load':
         {'compatible_conn': ['ssh'],
          'compatible_os': ['linux'],
           'modules':
            {'load':
             {'imported': <module 'modules.monitoring_modules.load' from '/home/aguilbaud/mum/app/modules/monitoring_modules/load.pyc'>,
              'external': False}
             }
            },
            ...
          }, ...
        }
        """
        return self.loaded_mod_moni

    def get_info_mod_monitoring(self):
        """
        Get the necessary informations about the monitoring modules part for the web application side.
        :return: a dictionary containing the monitoring informations:
        {
          part_mod_name:
            {
                'compatible_os': [val1, val2, ...],    => a list containing the compatibles os
                'compatible_conn': [val1, ...]         => a list containing the monitoring modules compatibles
                'unit': val,                           => the unit type of return ('%', 'bool' or other)
                'block': val,                          => the monitoring block of the module
            }
        }
        """
        res = {}
        for part in self.loaded_mod_moni:
            res[part] = {}
            res[part]['compatible_os'] = self.loaded_mod_moni[part]['compatible_os']
            res[part]['compatible_conn'] = self.loaded_mod_moni[part]['compatible_conn']
            res[part]['default'] = self.loaded_mod_moni[part]['default']
            res[part]['param'] = self.loaded_mod_moni[part]['param']
            mod_sample = self.loaded_mod_moni[part]['modules'].keys()[0]
            loaded_mod_sample = self.loaded_mod_moni[part]['modules'][mod_sample]['imported']
            res[part]['unit'] = getattr(loaded_mod_sample, 'unit')
            res[part]['block'] = getattr(loaded_mod_sample, 'block')
        return res

    def load_all_connection_modules(self):
        """
        Instanciates and stores the informations about each connection modules avaliable
        on the loaded_mod_conn attribute.
        """
        for importer, mod_name, ispkg in pkgutil.iter_modules(["app/modules/connection_modules/"]):
            if mod_name not in sys.modules:
                try:
                    loaded_mod = __import__("modules.connection_modules." + mod_name, fromlist=[mod_name])
                    class_name = getattr(loaded_mod, "get_class_name")()
                    mod_inst = getattr(loaded_mod, class_name)(None, None, None, None)
                    infos_mod = {}
                    infos_mod['imported'] = loaded_mod
                    infos_mod['class_name'] = getattr(mod_inst, 'get_name')()
                    infos_mod['params'] = getattr(mod_inst, 'get_parameters')()
                    infos_mod['known_port'] = getattr(mod_inst, 'get_known_port')()
                    self.loaded_mod_conn[mod_name] = infos_mod
                except AttributeError:
                    self.logger.warning("Error : internal connection module " + mod_name + " could not have been loaded.")
                    self.logger.debug(traceback.format_exc())

    def get_conection_modules_list(self):
        """
        Get informations about of the connection modules loaded and their parameters necessary to instanciate the
        connection. The type of the parameters is necessary for the web application in order to show a corresponding
        form (can be 'string' for a textfield, 'int' for a number field, 'file' for a file location).
        :return: a dictionary containing these informations on the form :
        {
          mod_name:
            {
                'class_name': val,                     => the name of the class to instanciate
                'imported': module,                    => the module imported
                'params': {param1: type1, param2: type2, ...}       => the parameters necessary to create the connection
            }
        }
        """
        return self.loaded_mod_conn

    def get_info_mod_conn(self):
        """
        Get informations about of the connection modules loaded and their parameters necessary to instanciate the
        connection. The type of the parameters is necessary for the web application in order to show a corresponding
        form (can be 'string' for a textfield, 'int' for a number field, 'file' for a file location).
        :return: a JSON string containing informations about the connection modules:
        {
          mod_name:
            {
                'params': {param1: type1, param2: type2, ...}       => the parameters necessary to create the connection
                'known_port': int                                   => the usual port used for this connection
            }
        }
        """
        res = {}
        for mod in self.loaded_mod_conn:
                res[mod] = self.loaded_mod_conn[mod]['params']
                res[mod]['known_port'] = self.loaded_mod_conn[mod]['known_port']
        return res

    def load_all_notification_modules(self):
        """
        Instanciates and stores the informations about each notification modules avaliable
        on the loaded_mod_notif attribute.
        """
        for importer, mod_name, ispkg in pkgutil.iter_modules(["app/modules/notification_modules/"]):
            if mod_name not in sys.modules and not mod_name == 'websocket_container':
                try:
                    loaded_mod = __import__("modules.notification_modules." + mod_name, fromlist=[mod_name])
                    class_name = getattr(loaded_mod, "get_class_name")()
                    mod_inst = getattr(loaded_mod, class_name)(None, None, None, None)
                    infos_mod = {}
                    infos_mod['imported'] = loaded_mod
                    infos_mod['class_name'] = getattr(mod_inst, 'get_name')()
                    self.loaded_mod_notif[mod_name] = infos_mod
                except AttributeError:
                    self.logger.warning("Error : internal notification module " + mod_name + " could not have been loaded.")
                    self.logger.debug(traceback.format_exc())

    def get_info_mod_notification(self):
        """
        Get informations about of the connection modules loaded and their parameters necessary to instanciate the
        connection. The type of the parameters is necessary for the web application in order to show a corresponding
        form (can be 'string' for a textfield, 'int' for a number field, 'file' for a file location).
        :return: a JSON string containing informations about the connection modules:
        {
          mod_name:
            {
                'params': {param1: type1, param2: type2, ...}       => the parameters necessary to create the connection
            }
        }
        """
        res = {}
        for mod in self.loaded_mod_notif:
            res[mod] = ''
        return res

    def run_notification_modules(self, dict_notif):
        """
        Runs the notifications modules specified in the dict_notif parameter. (called by run_one_monitoring_module)
        :param dict_notif:
        {
          notif_mod: [{'username': string, 'moni_mod': string, 'title': string, 'msg': string}, ...],
          ...
        }
        :return:
        """
        for notif_mod in dict_notif:
            while len(dict_notif[notif_mod]) > 0:
                instr = dict_notif[notif_mod].pop()
                mod_inst = getattr(self.loaded_mod_notif[notif_mod]['imported'],
                                   self.loaded_mod_notif[notif_mod]['class_name'])(instr['user'],
                                                                                   instr['title'],
                                                                                   instr['msg'],
                                                                                   self.conf)
                try:
                    mod_inst.notify()
                except KeyError:
                    self.logger.warning('Missing setting for notification module ' + notif_mod)
                    self.logger.debug(traceback.format_exc())
                except Exception:
                    self.logger.error('An error occured with notification module ' + self.loaded_mod_notif[notif_mod]['class_name'])
                    self.logger.error(traceback.format_exc())

    def update_global_conf(self):
        """
        Asks the database to update the configuration if new monitoring modules are added in function of the
        modules informations stored.
        """
        self.db.create_global_conf(self.loaded_mod_moni)

    def get_all_monitoring_instructions(self):
        """
        runs on the database the function get_monitoring_instructions for all hosts under monitoring
        :return:the monitoring instructions for all hosts
        """
        res = []
        for addr_host in self.db.get_list_addr_hosts():
            for instr in self.db.get_monitoring_instructions(addr_host):
                res.append(instr)
        return res

    def launch_db_function(self, dict_instr):
        """
        Calls a function of the database with the parametters sent. Used to each function which is not module dependant:
        remove host, create/remove group, add/remove to group, save settings, etc.
        :param dict_instr: a dictionary containing :
        {
            "func" : func_name,
            "args" : [arg1, ...]
        }
        :return: the result of the called function. Or None if the called function does not returns a value.
        """
        return getattr(self.db, dict_instr['func'])(dict_instr['args'])

    def set_idling_state(self, addr_host):
        """

        :param addr_host:
        :return:
        """
        dict_instr = self.db.set_monitoring_activation(addr_host)
        if "rem" in dict_instr:
            process_monitoring.remove_to_waiting_list(addr_host, None)
        elif dict_instr['add'] != []:
            for instr in dict_instr['add']:
                process_monitoring.add_to_waiting_list(instr)

    def update_activated_modules(self, args):
        """
        Save on the database, the new configuration concerning the activation/deactivation
        of monitoring modules for a host.
        Then, it creates a dictionary containing instruction about what check to add/remove on the process_monitoring
        waiting list.
        :param args: a dictionary containing:
        {
            'addr_host': string,
            'activated':{
                mod_name: bool
            }
        }
        """
        dict_instr = self.db.config_mod_activation(args)
        for add_instr in dict_instr['add']:
            process_monitoring.add_to_waiting_list(add_instr)
        for rem_instr in dict_instr['rem']:
            process_monitoring.remove_to_waiting_list(rem_instr['addr'], rem_instr['mod_name'])

    def set_conf_moni_mod(self, args):
        """
        Save on the database the new configuration of a monitoring module for a given host.
        Then, it updates the process_monitoring waiting list with the new configuration.
        :param args: a dictionary containing :
        {
            'addr_host': string,
            'mod_name': string,
            'freq' : int,
            'minor_limit' : int,
            'major_limit' : int
        }
        """
        self.db.set_conf_mod(args)
        process_monitoring.update_mod_on_waiting_list(args)

    def get_public_keys_list(self):
        """
        Get the content of the directory specified on the conf['keys_location'] parameter of the application.
        :return: a list containing the files on the conf['keys_location']
        """
        return os.listdir(self.conf['keys_location'])

    def remove_host(self, args):
        """
        Removes all informations concerning the given host from the database.
        Also remove each instructions concerning the given host from the process_monitoring waiting list.
        :param args: a dictionary containing :
        {'addr_host': string}
        """
        self.db.remove_host(args)
        # removing monitoring instructions for this host
        process_monitoring.remove_to_waiting_list(args['addr_host'], None)

    def get_host_info(self, addr_host):
        """
        Asks from the database the informations concerning a given host.
        Add to these informations the compatible_os_list attribute.
        :param addr_host: the IP address of the host
        :return: a dictionary containing the result of get_host_informations() + the attribute compatible_os_list
        """
        host_info = self.db.get_host_informations(addr_host)
        host_info['compatible_os_list'] = self.compatible_os_list
        return host_info

    def get_logs(self, log_size):
        """
        Will exec the tail on the log file and return the content.
        :param log_size: the n last number of the file to get
        :return: the log_size last lines of the log file
        """
        log_location = self.conf['log_location']
        child = pexpect.spawn('tail', [log_location, '-n', str(log_size)])
        child.expect(pexpect.EOF)
        return child.before
