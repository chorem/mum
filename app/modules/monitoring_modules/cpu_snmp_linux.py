# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "CPU"
name = "CPU SNMP"
unit = "%"
connection = "snmp"


def check(conn, mnce, subparts):
    """
    Returns the greatest between the user and system CPU charge
    """
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.11.9.0
    oid_user_cpu_charge = ".1.3.6.1.4.1.2021.11.9.0"
    user_cpu_charge = int(conn.exec_command(oid_user_cpu_charge))
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.11.10.0
    oid_system_cpu_charge = ".1.3.6.1.4.1.2021.11.10.0"
    system_cpu_charge = int(conn.exec_command(oid_system_cpu_charge))
    res_cpu = max(user_cpu_charge, system_cpu_charge)
    return res_cpu