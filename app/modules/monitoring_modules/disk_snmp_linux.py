# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Disk"
name = "Disk SNMP"
unit = "%"
connection = "snmp_walk"

param = {
    "Disk": {'type': 'detection', 'value': ['/'], 'doc': "The partitions to verify"}
}

def check(conn, mnce, subparts):
    res_check = {}

    if subparts == []:
        subparts.append('/')

    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.9.1.2
    oid_mounted_partitions = "1.3.6.1.4.1.2021.9.1.2"
    mounted_partitions = conn.exec_command(oid_mounted_partitions)
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.9.1.9
    oid_percent_used = "1.3.6.1.4.1.2021.9.1.9"
    disk_used_on_partitions = conn.exec_command(oid_percent_used)

    for partition in mounted_partitions:
        if mounted_partitions[partition] in subparts:
            oid_nodes = partition.split('.')
            partition_node_number = oid_nodes[len(oid_nodes) - 1]
            res_check[mounted_partitions[partition]] = \
                int(disk_used_on_partitions["1.3.6.1.4.1.2021.9.1.9." + partition_node_number])

    return res_check