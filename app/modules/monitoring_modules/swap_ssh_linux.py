# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Swap"
name = "Swap SSH"
unit = "%"
connection = "ssh"


def check(conn, mnce, subparts):
    cmd = "free -m"
    stdout = conn.exec_command(cmd)

    # output example:
    """
             total       used       free     shared    buffers     cached
Mem:          5781       1995       3786          0         63        664
-/+ buffers/cache:       1267       4513
Swap:         7724          0       7724

    """

    swap_total = 0
    swap_used = 0
    for line in stdout.splitlines():
        fields = line.split()
        if fields[0] == 'Swap:':
            swap_total = fields[1]
            swap_used = fields[2]

    try:
        res_swap = (int(swap_used) * 100) / int(swap_total)
    except ZeroDivisionError:
        exception_inst = getattr(mnce, "ModuleNotCompatibleException")(
            part, conn.get_addr_host()
        )
        raise exception_inst

    return res_swap