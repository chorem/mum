# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import pexpect

compatible_os = ["all"]
block = "network"
part = "Ping"
name = part
unit = "bool"
connection = ""

default = {
    "check_frequency": 420
}

def check(addr_host, port_list, cnfe):
    res_check = False
    try:
        child = pexpect.spawn('ping ' + addr_host + ' -c 1', env={'LANG': 'C'})
        while child.isalive():
            child.expect('packets transmitted, ')
        child.expect(' received')
        res = child.before
        res_check = res == "1"
    except pexpect.EOF:
        res_check = False
    except pexpect.TIMEOUT:
        res_check = False
    finally:
        return res_check