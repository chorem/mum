# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import urllib2

compatible_os = ["all"]
block = "network"
part = "HTTP"
name = part
unit = "bool"
connection = ""


def check(addr_host, port_list, cnfe):
    res_http_check = {}
    http_port_found = False

    for i in range(len(port_list)):
        # for each http port detected for this host
        if port_list[i]['portname'] == "http":
            http_port_found = True
            try:
                # the result of the check is true if urlopen returns the http code 200
                res_http_check[port_list[i]['portid']] = urllib2.urlopen("http://" + addr_host + ":" + str(port_list[i]['portid']),
                                                         None,
                                                         10
                                                         ).getcode() == 200 or 401
            except Exception:
                res_http_check[port_list[i]['portid']] = False
    if not http_port_found:
        # if there is no http port detected for this host
        exception_inst = getattr(cnfe, "CommandNotFoundException")(
            part, addr_host
        )
        raise exception_inst
    return res_http_check
