# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Load 15 min"
name = "Load 15 min SSH"
unit = ""
connection = "ssh"


def check(conn, mnce, subparts):
    """
    Returns the greatest between the user and system CPU charge
    """
    cmd = "top -b -n 1"
    stdout = conn.exec_command(cmd)

    # output example:
    """
    top - 09:35:23 up 7 min,  2 users,  load average: 0,25, 0,51, 0,32
Tasks: 179 total,   1 running, 178 sleeping,   0 stopped,   0 zombie
%Cpu(s): 10,0 us,  1,2 sy,  0,0 ni, 76,4 id, 12,1 wa,  0,0 hi,  0,2 si,  0,0 st
KiB Mem:   5920240 total,  1952176 used,  3968064 free,    63364 buffers
KiB Swap:  7910396 total,        0 used,  7910396 free,   658164 cached

  PID USER      PR  NI  VIRT  RES  SHR S  %CPU %MEM    TIME+  COMMAND
    1 root      20   0 10660 1632 1496 S   0,0  0,0   0:00.95 init
    2 root      20   0     0    0    0 S   0,0  0,0   0:00.00 kthreadd
    3 root      20   0     0    0    0 S   0,0  0,0   0:00.06 ksoftirqd/0
    [...]
    """

    load_avg = 0.0
    for line in stdout.splitlines():
        fields = line.split()
        if not fields == []:
            if fields[0] == 'top':
                for i in range(len(fields)):
                    if fields[i] == 'average:':
                        field_load_avg = fields[i+3]
                        field_load_avg = field_load_avg.replace(',', '.')
                        load_avg = float(field_load_avg)
    return load_avg