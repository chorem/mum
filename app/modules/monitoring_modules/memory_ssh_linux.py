# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import re


compatible_os = ['linux']
block = "hardware"
part = "Memory"
name = "Memory SSH"
unit = "%"
connection = "ssh"


def check(conn, mnce, subparts):
    # returns the percentage of used RAM

    cmd = "cat /proc/meminfo"
    stdout = conn.exec_command(cmd)

    # output example:
    """
    MemTotal:        5920240 kB
    MemFree:         3894920 kB
    MemAvailable:    4408708 kB
    [...]
    """

    memfree = 0
    memtotal = 0
    for line in stdout.splitlines():
        tab_res = line.split(':')
        if(tab_res[0]) == 'MemTotal':
            memtotal = re.sub("[^0-9]", "", tab_res[1])
        elif(tab_res[0]) == 'MemFree':
            memfree = re.sub("[^0-9]", "", tab_res[1])
    memused = int(memtotal) - int(memfree)

    if memused <= 0:
        exception_inst = getattr(mnce, "ModuleNotCompatibleException")(
            part, conn.get_addr_host()
        )
        raise exception_inst

    res_check = memused * 100 / int(memtotal)
    return res_check