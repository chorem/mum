# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import re


compatible_os = ['linux']
block = "hardware"
part = "Disk"
name = "Disk SSH"
unit = "%"
connection = "ssh"

# if detection, name must be the SAME as the name of the part attribute on the detection module
param = {
    "Disk": {'type': 'detection', 'value': ['/'], 'doc': "The partitions to verify"}
}

def check(conn, mnce, subparts):
    cmd = "df -h"
    stdout = conn.exec_command(cmd)

    # output example:
    """
    Filesystem                                              Size  Used Avail Use% Mounted on
    rootfs                                                    55G   11G   42G  21% /
    udev                                                      10M     0   10M   0% /dev
    tmpfs                                                    579M  1,1M  578M   1% /run
    /dev/disk/by-uuid/4fd2e184-118c-40d3-8ed0-9e288a96e155    55G   11G   42G  21% /
    tmpfs                                                    5,0M     0  5,0M   0% /run/lock
    tmpfs                                                    2,7G   92K  2,7G   1% /run/shm
    /dev/sda3                                                 28G  4,3G   22G  17% /home
    /dev/sda4                                                 20G   44M   19G   1% /var/local
    """

    disk_used = None
    ignore = True
    for line in stdout.splitlines():
        # we ignore the first line which contains no value
        if ignore:
            ignore = False
        else:
            values = line.split()
            if values[len(values)-1] == "/":
                disk_used = re.sub("[^0-9]", "", values[len(values)-2])
    if disk_used is None:
        exception_inst = getattr(mnce, "ModuleNotCompatibleException")(
            part, conn.get_addr_host()
        )
        raise exception_inst
    res_check = int(disk_used)
    return res_check