# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

import re

__author__ = 'aguilbaud'


compatible_os = ['linux']
block = "software"
part = "Updated packages with APT"
name = "Updated packages with APT (SSH)"
unit = "bool"
connection = "ssh"


def check(conn, mnce, subparts):
    cmd = "apt-get upgrade -s"
    stdout = conn.exec_command(cmd)

    # Output example:
    """
    NOTE: This is only a simulation!
          apt-get needs root privileges for real execution.
          Keep also in mind that locking is deactivated,
          so don't depend on the relevance to the real current situation!
    Reading package lists... Done
    Building dependency tree
    Reading state information... Done
    The following packages will be upgraded:
      linux-image-3.16.0-0.bpo.4-amd64 linux-image-3.2.0-4-amd64 linux-libc-dev p7zip-full tzdata tzdata-java
    6 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
    Inst linux-image-3.16.0-0.bpo.4-amd64 [3.16.7-ckt9-3~deb8u1~bpo70+1] (3.16.7-ckt11-1~bpo70+1 Debian Backports:/wheezy-backports [amd64])
    Inst linux-image-3.2.0-4-amd64 [3.2.68-1+deb7u1] (3.2.68-1+deb7u2 Debian-Security:7.0/oldstable [amd64])
    """

    res_check = re.search("The following packages will be upgraded:", stdout) is not None
    return res_check
