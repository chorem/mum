# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Swap"
name = "Swap SNMP"
unit = "%"
connection = "snmp"


def check(conn, mnce, subparts):
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.4.3.0
    oid_total_swap_size = ".1.3.6.1.4.1.2021.4.3.0"
    total_swap = float(conn.exec_command(oid_total_swap_size))
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.4.4.0
    oid_avaliable_swap_space = ".1.3.6.1.4.1.2021.4.4.0"
    swap_used = total_swap - float(conn.exec_command(oid_avaliable_swap_space))
    try:
        percent_swap_used = round( (swap_used * 100) / total_swap, 2)
    except ZeroDivisionError:
        exception_inst = getattr(mnce, "ModuleNotCompatibleException")(
            part, conn.get_addr_host()
        )
        raise exception_inst
    return percent_swap_used