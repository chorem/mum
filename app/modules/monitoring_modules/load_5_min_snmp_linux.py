# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Load 5 min"
name = "Load 5 min SNMP"
unit = ""
connection = "snmp"


def check(conn, mnce, subparts):
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.10.1.3 (.X)
    # X = 1 : 5 min load
    # X = 2 : 10 min load
    # X = 3 : 15 min load
    oid_5_min_load = ".1.3.6.1.4.1.2021.10.1.3.1"
    load = float(conn.exec_command(oid_5_min_load))
    return load