# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux']
block = "hardware"
part = "Memory"
name = "Memory SNMP"
unit = "%"
connection = "snmp"


def check(conn, mnce, subparts):
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.4.5.0
    oid_total_ram_avaliable = ".1.3.6.1.4.1.2021.4.5.0"
    total_mem = float(conn.exec_command(oid_total_ram_avaliable))
    # http://www.oid-info.com/get/1.3.6.1.4.1.2021.4.6.0
    oid_total_ram_used = ".1.3.6.1.4.1.2021.4.6.0"
    mem_used = float(conn.exec_command(oid_total_ram_used))
    try:
        percent_mem_used = round((mem_used * 100) / total_mem, 2)
    except ZeroDivisionError:
        exception_inst = getattr(mnce, "ModuleNotCompatibleException")(
            part, conn.get_addr_host()
        )
        raise exception_inst
    return percent_mem_used