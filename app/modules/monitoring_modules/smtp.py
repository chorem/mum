# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import smtplib

compatible_os = ["all"]
block = "network"
part = "SMTP"
name = part
unit = "bool"
connection = ""


def check(addr_host, port_list, cnfe):
    smtp_port_found = False
    res_check = {}

    for i in range(len(port_list)):
        # for each smtp port detected for this host
        if port_list[i]['portname'] == "smtp":
            smtp_port_found = True
            # try to connect to this port, with a timeout of 10 seconds
            try:
                smtp_conn_inst = smtplib.SMTP(addr_host, int(port_list[i]['portid']), 10)
                # send a HELO request and should return a tuple with 250 as first member if OK
                res_check[port_list[i]['portid']] = smtp_conn_inst.helo()[0] == 250
                # close the connection when finished
                smtp_conn_inst.quit()
            except Exception:
                res_check[port_list[i]['portid']] = False

    if not smtp_port_found:
        # if there is no smtp port detected for this host
        exception_inst = getattr(cnfe, "CommandNotFoundException")(
            part, addr_host
        )
        raise exception_inst
    return res_check
