# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

from pysnmp.entity.rfc3413.oneliner import cmdgen
import logging


def get_class_name():
    return "SNMPWalk"


class SNMPWalk:
    def __init__(self, addr_host, params, key_loc, cnfe):
        self.parameters = {"port": "int"}
        self.params_stored = params
        self.name = get_class_name()
        self.addr_host = addr_host
        self.known_port = 161
        self.CommandNotFoundException = cnfe
        self.cmdGen = cmdgen.CommandGenerator()
        self.logger = logging.getLogger("mum_log")

    def get_name(self):
        return self.name

    def get_addr_host(self):
        #  Called by monitoring modules
        return self.addr_host

    def get_parameters(self):
        return self.parameters

    def get_known_port(self):
        return self.known_port

    def exec_command(self, cmd):
        res = {}
        errorIndication, errorStatus, errorIndex, varBindTable = self.cmdGen.nextCmd(
            cmdgen.CommunityData('public'),
            cmdgen.UdpTransportTarget((self.addr_host, int(self.params_stored['port']))),
            cmd
        )

        # Check for errors and print out results
        if errorIndication:
            self.logger.warning(errorIndication)
            exception_inst = getattr(self.CommandNotFoundException, "CommandNotFoundException")(
                cmd, self.addr_host
            )
            raise exception_inst
        else:
            if errorStatus:
                self.logger.warning('%s at %s' % (
                    errorStatus.prettyPrint(),
                    errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
                ))
                exception_inst = getattr(self.CommandNotFoundException, "CommandNotFoundException")(
                    cmd, self.addr_host
                )
                raise exception_inst
            else:
                for varBindTableRow in varBindTable:
                    for name, val in varBindTableRow:
                        if val.prettyPrint() != "":
                            # if this oid returned a result
                            res[name.prettyPrint()] = val.prettyPrint()
                if res == {}:
                    # we got nothing for this oid => it doens't exists on this host
                    exception_inst = getattr(self.CommandNotFoundException, "CommandNotFoundException")(
                        cmd, self.addr_host
                    )
                    raise exception_inst
        return res

    def disconnect(self):
        self.cmdGen = None