# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

import paramiko


def get_class_name():
    return "SSH"


class SSH:
    def __init__(self, addr_host, params, key_loc, cnfe):
        self.parameters = {"username": "string", "password": "string", "private_key": "file", "port": "int"}
        self.name = get_class_name()
        self.addr_host = addr_host
        self.known_port = 22
        self.CommandNotFoundException = cnfe

        if params is not None:
            key_path = str(key_loc) + str(params['private_key'])
            key = paramiko.RSAKey.from_private_key_file(key_path)

            if 'password' not in params or params['password'] == "":
                params['password'] = None

            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(addr_host,
                             username=params['username'],
                             password=params['password'],
                             pkey=key,
                             port=params['port'])

    def get_name(self):
        return self.name

    def get_addr_host(self):
        #  Called by monitoring modules
        return self.addr_host

    def get_parameters(self):
        return self.parameters

    def get_known_port(self):
        return self.known_port

    def exec_command(self, cmd):
        stdin, stdout, stderr = self.ssh.exec_command("bash -c 'LANG=C %s'" % cmd)
        out = stdout.read()
        err = stderr.read()
        if not err == "" and out == "":
            exception_inst = getattr(self.CommandNotFoundException, "CommandNotFoundException")(
                cmd, self.addr_host
            )
            raise exception_inst
        return out

    def disconnect(self):
        self.ssh.close()