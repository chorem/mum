# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"


__author__ = 'aguilbaud'

from datetime import datetime
from datetime import timedelta
from sys import maxint
import json
import shelve
import traceback
import threading
import logging

import os.path


class shelve_db:
    """
    Storage module for the persistant objects in Python : Shelve.
    Every function in need to access the database have to be implemented on this class.
    """
    def __init__(self, db_loc):
        self.db = None
        self.lock = threading.Lock()
        self.db_loc = db_loc
        self.logger = logging.getLogger("mum_log")

    def open_db(self):
        """
        Open the shelve database from the file specified in conf.txt.
        If the file donesn't exists, it will be created and the first structure will also be initialized.
        Moreover, a lock is activated during all the transaction, because Shelve is not thread safe.
        """
        self.lock.acquire()
        if not os.path.isfile(self.db_loc):  # init of the database at the first opening
            self.db = shelve.open(self.db_loc, writeback=True)
            try:
                self.db["hosts"] = {}
                self.db["users"] = {}
                self.db["groups"] = {}
                self.db["global_conf"] = {}
                self.db["tasks"] = []
                self.db["version"] = "0.1"
            except:
                self.logger.error("Database initilalization error")
                self.db.close()
                self.lock.release()
        else:
            self.db = shelve.open(self.db_loc, writeback=True)

    def close_db(self):
        """
        Closes the database and release the lock.
        """
        self.db.close()
        self.db = None
        self.lock.release()

    def get_db_version(self):
        self.open_db()
        res = None
        try:
            res = self.db['version']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def set_db_version(self, new_version):
        self.open_db()
        try:
            self.db['version'] = new_version
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
    """
    def add_field(self, new_val_name, dict_instr):
        self.open_db()
        path = dict_instr['path']
        new_val = dict_instr['new_val']
        path_tab = []
        try:
            for i in range(len(path)):
                if path[i] == '*':

        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()


    def add_field_req(self, path_list, pos_path, path, field_to_add_name, field_to_add_val):
        if pos_path < len(path):
            current_key = str(path[pos_path])
            if current_key == '*':
                print path
                for key in self.get_nested_default(dict(self.db), path, None):
                    path[pos_path] = key
                    return self.add_field_req(path_list, pos_path + 1, path, field_to_add_name, field_to_add_val)
            else:
                return self.add_field_req(path_list, pos_path + 1, path, field_to_add_name, field_to_add_val)
        else:
            if pos_path == 0:
                path_list.append('')
                return path_list
            else:
                path_list.append(path)
                return path_list
    """


    @staticmethod
    def get_nested_default(d, path, new_elem):
        return reduce(lambda d, k: d.setdefault(k, new_elem), path, d)

    def reset_tasks(self):
        self.open_db()
        try:
            self.db['tasks'] = []
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def init_global_conf(self, loaded_mod_moni):
        """
        This method is executed once at each launch of the application.
        It creates an entrey on db['global_conf'] for each new loaded monitoring module.
        If an entry exists for a non loaded module, it will be removed.
        :param loaded_mod_moni: a dictionary containing :
        {
          mod_name:
            {
                'class_name': string,                     => the name of the class to instanciate
                'compatible_os': [string1, string2, ...], => a list containing the compatibles os
                'unit': string,                           => the unit type of return ('%', 'bool' or other)
                'block': string,                          => the monitoring block of the module
                'external': bool                          => indicates if this modules comes from external directory
            }
        }
        """
        self.open_db()
        try:

            for mod in loaded_mod_moni:
                if mod not in self.db['global_conf']:  # adding a entry for every module loaded for the first time
                    mod_conf = {}
                    mod_conf['block'] = loaded_mod_moni[mod]['block']
                    # all modules are added if the os is compatible, we'll try at least
                    # once the check, if it exists a connection that can lauch the module
                    mod_conf['activated'] = True
                    mod_conf['check_frequency'] = 600
                    mod_conf['nb_min'] = 59
                    mod_conf['nb_hour'] = 23
                    mod_conf['nb_day'] = 7
                    mod_conf['nb_week'] = 4
                    mod_conf['nb_month'] = 12
                    mod_conf['nb_year'] = None
                    mod_conf['subparts'] = []  # a list that can contain for example the names of the disk partitions
                    unit = loaded_mod_moni[mod]['unit']
                    mod_conf['unit'] = unit
                    if unit == '%':
                        mod_conf['minor_limit'] = 90
                        mod_conf['major_limit'] = 98
                    elif unit == 'bool':
                        mod_conf['minor_limit'] = False
                        mod_conf['major_limit'] = True
                    else:
                        mod_conf['minor_limit'] = 8
                        mod_conf['major_limit'] = 10
                    if 'param' in loaded_mod_moni[mod]:
                        mod_conf['param'] = loaded_mod_moni[mod]['param']
                    for default_val in loaded_mod_moni[mod]['default']:
                        mod_conf[default_val] = loaded_mod_moni[mod]['default'][default_val]
                    mod_conf['subscribers'] = {}
                    self.db['global_conf'][mod] = mod_conf
            # removing entries of modules that are no loaded anymore
            mods_to_del = []
            for mod in self.db['global_conf']:
                if mod not in loaded_mod_moni:
                    mods_to_del.append(mod)
            for mod in mods_to_del:
                del self.db['global_conf'][mod]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_global_settings(self, args):
        """
        Asked from the global settings configuration page.
        :param args: None (for dynamic call)
        :return: the content of db['global_conf']
        """
        res = {}
        self.open_db()
        try:
            res = self.db['global_conf']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def set_global_settings(self, args):
        """
        Asked from the global settings configuration page. Updates the default configuration of a monitoring module.
        :param args: A dictionnary containing :
        {
            'mod_name': str,
            'minor_limit': val,
            'major_limit': val,
            'freq': int
        }
        """
        self.open_db()
        try:
            self.db['global_conf'][args['mod_name']]['minor_limit'] = args['minor_limit']
            self.db['global_conf'][args['mod_name']]['major_limit'] = args['major_limit']
            self.db['global_conf'][args['mod_name']]['check_frequency'] = args['freq']
            for period in ['min', 'hour', 'day', 'week', 'month', 'year']:
                if args['nb_' + period] == '':
                    self.db['global_conf'][args['mod_name']]['nb_' + period] = None
                else:
                    self.db['global_conf'][args['mod_name']]['nb_' + period] = args['nb_' + period]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_subpart(self, addr_host, modname):
        """
        Asked while launching a monitoring module.
        :param addr_host: The IP adress of the host
        :param modname: the name of the monitoring module
        :return: a list containing subparts to check, if configured
        """
        self.open_db()
        res = []
        try:
            res = self.db['hosts'][addr_host]['conf']['monitoring'][modname]['subparts']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def set_subpart(self, args):
        """
        Asked from the hostpage. Set the subpart list to monitore.
        :param args: A dictionary containing:
        {
            'addr_host': str,
            'modname': str,
            'subpart_list': [str, str, ...]
        }
        """
        self.open_db()
        try:
            self.db['hosts'][args['addr_host']]['conf']['monitoring'][args['modname']]['subparts'] = \
                args['subpart_list']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def add_host(self, addr_host, nmap_res, conn_infos, dict_mod_info):
        """
        Called by the nmap_detection module or directly by the module loader if no detection was asked.
        Add and save a new host after its first nmap detection
        It also preconfigure with the default configuration, add the host to the group "all" and
        creates empty structures for the monitoring and archive data.
        :param addr_host: the IP adress of the host to add
        :param nmap_res: a string containing the json reslult of the nmap detection of this host
        :param conn_infos: a dictionary containing informations about the different connection modules
        (see get_conection_modules_list() on module_loader)
        :param dict_mod_info: a dictionnary containing informations about the different monitoring modules
        (see get_info_mod_monitoring() on module_loader)
        """
        self.open_db()
        addr_host = str(addr_host)   # Shelve doesn't support Unicode
        try:
            if addr_host in self.db['hosts']:
                # it's rescan, we update only nmap related results
                self.db["hosts"][addr_host]["detected"]["nmap"] = nmap_res
                nmap_res_data = json.loads(nmap_res)
                self.db["hosts"][addr_host]["conf"]["connections"] = self.init_conn(nmap_res_data, conn_infos)
                os_host = nmap_res_data['os']
                for mod in dict_mod_info:
                    if os_host in dict_mod_info[mod]['compatible_os'] or 'all' in dict_mod_info[mod]['compatible_os']:
                        self.db["hosts"][addr_host]["conf"]["monitoring"][mod] = self.db['global_conf'][mod]
            else:
                #  Add the nmap detection
                self.db["hosts"][addr_host] = {}
                self.db["hosts"][addr_host]["detected"] = {}
                #  Preconfiguration
                self.db["hosts"][addr_host]["conf"] = {}
                nmap_res_data = json.loads(nmap_res)
                self.db["hosts"][addr_host]['detected']['nmap'] = nmap_res
                self.db["hosts"][addr_host]["conf"]["connections"] = self.init_conn(nmap_res_data, conn_infos)
                os_host = nmap_res_data['os']
                if nmap_res_data['hostname'] != "":
                    self.db["hosts"][addr_host]["conf"]["display_name"] = nmap_res_data['hostname']
                else:
                    self.db["hosts"][addr_host]["conf"]["display_name"] = str(addr_host)
                self.db["hosts"][addr_host]["conf"]["monitoring"] = {}
                for mod in dict_mod_info:
                    if os_host in dict_mod_info[mod]['compatible_os'] or 'all' in dict_mod_info[mod]['compatible_os']:
                        self.db["hosts"][addr_host]["conf"]["monitoring"][mod] = self.db['global_conf'][mod]
                self.db['hosts'][addr_host]['conf']['idling'] = {}      # For reactivation after an idle state
                self.db['hosts'][addr_host]['conf']['idling']['modules'] = []
                self.db['hosts'][addr_host]['conf']['idling']['status'] = ""
                self.db["hosts"][addr_host]["conf"]["groups"] = ["all"]  # Every host is in group "all"
                self.db["hosts"][addr_host]["conf"]["subscribers"] = {}
                self.db["hosts"][addr_host]["conf"]["custom_info"] = ""
                self.db["hosts"][addr_host]["conf"]["interventions"] = []
                #  Create structure for monitoring data
                self.db["hosts"][addr_host]["monitoring"] = {}
                #  Create structure for global status of host
                self.db["hosts"][addr_host]["status"] = {}
                self.db["hosts"][addr_host]["status"]["state"] = ""
                #  Create structure for archiving data
                self.db["hosts"][addr_host]["archive"] = {}
                self.db["hosts"][addr_host]["stats"] = {}
                #self.db["hosts"][addr_host]['param']['port'] = nmap_res_data['openports']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    @staticmethod
    def init_conn(dict_nmap_res, conn_infos):
        """
        Returns an initialization for the connection configuration on a host.
        :param dict_nmap_res: The result of the nmap detection in a dictionnary:
        :param conn_infos: A dictionnary containing informations about connection modules in the form :
        {
          mod_name: {
            'params': {param1: type1, param2: type2, ...}
            'known_port': int
          }
        }
        :return: a dictionary containing:
        {conn_mod_name: {'priority': int, 'port': int, param1: None, param2: None, ...}, ...}
        """
        dict_conn = {}
        for port in dict_nmap_res['openports']:
            for loaded_conn_mod in conn_infos:
                if conn_infos[loaded_conn_mod]['known_port'] == int(port['portid']):
                    dict_conn[loaded_conn_mod] = {}
                    if len(conn_infos[loaded_conn_mod]['params'].keys()) == 1 and \
                                    'port' in conn_infos[loaded_conn_mod]['params']:
                        # if there is only the port to configure, the conn module can be activated because is
                        # already configured
                        dict_conn[loaded_conn_mod]["priority"] = 1
                    else:
                        for param in conn_infos[loaded_conn_mod]['params']:
                            dict_conn[loaded_conn_mod][param] = None
                        dict_conn[loaded_conn_mod]["priority"] = 0
                    dict_conn[loaded_conn_mod]["port"] = conn_infos[loaded_conn_mod]['known_port']
        return dict_conn

    def get_conn_param(self, args):
        """
        Called from the hostpage.
        Returns the connection parameters of an host.
        :param args: A dictionnary containing :
        {'addr_host': string}
        :return: The connection parameters in the form:
          mod_name: {
            {param1: string1, param2: string2, ...}
          }
        """
        self.open_db()
        res = None
        try:
            res = self.db['hosts'][args['addr_host']]['conf']['connections']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_monitoring_instructions(self, addr_host):
        """
        Necessary to launch for the first time all activated monitoring modules for a given host
        :param addr_host: the address IP of the host
        :return: structured informations about monitoring in form :
        [{
            'addr' : string,       => the IP address of the host
            'mod_name', string,    => the name of the monitoring module
            'time', datetime,      => the time at when to launch the monitoring module
            'freq', int            => the frequency check (in seconds)
        }, ...]
        """
        self.open_db()
        res = []
        try:
            for mod in self.db['hosts'][addr_host]['conf']['monitoring']:
                if self.db['hosts'][addr_host]['conf']['monitoring'][mod]['activated']:
                    dict_moni = {}
                    dict_moni['addr'] = addr_host
                    dict_moni['mod_name'] = mod
                    if mod in self.db['hosts'][addr_host]['monitoring']:
                        # the service have benn restarted, the next check will occure at last_check + check_freq
                        last_check = datetime.strptime(self.db['hosts'][addr_host]['monitoring'][mod]['date'],
                                                       '%Y-%m-%d %H:%M:%S.%f')
                        dict_moni['time'] = last_check + timedelta(seconds=self.db['hosts'][addr_host]['conf']['monitoring'][mod]['check_frequency'])
                    else:
                        # this is the first time this module will check for this host,
                        # the next check will occure right now
                        dict_moni['time'] = datetime.now()
                    dict_moni['freq'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod]['check_frequency']
                    res.append(dict_moni)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_list_addr_hosts(self):
        """
        Necessary when running again the application, for getting the monitoring instructions for all hosts
        :return: a list containing all the IP adresses of every hosts under monitoring
        """
        self.open_db()
        res = []
        try:
            for addr_host in self.db['hosts']:
                res.append(addr_host)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_hosts(self):
        """
        Returns the essential data about all hosts under monitoring.
        These are used by the front-end application.
        :return: a list containing the essential data about all hosts under monitoring on the form:
                [
            {
                "addr":"192.168.74.1",
                "name":"www.example.com",
                "status":string, //"success" or "warning" or "danger" or ""
                "group":[  "all", ...],
                "last_check":datetime,
                "subscribers":{
                             "uid":string,
                             "priority":int
                            },
                "warning": [mod_name, ...],
                "danger": [mod_name, ...],
                "nb_subscribers": int,
                "display_name": str
            },
            ...
        ]
        If no hosts have been added, the function will return an empty list.
        """
        self.open_db()
        res = []
        try:
            if self.db["hosts"] != {}:
                for host in self.db["hosts"]:
                    detected = json.loads(self.db["hosts"][host]["detected"]["nmap"])
                    info_host = {}
                    info_host["addr"] = detected["addr"]
                    if 'hostname' in detected:
                        info_host["name"] = detected["hostname"]
                    else:
                        info_host["name"] = ""
                    if "state" in self.db["hosts"][host]["status"]:
                        info_host["status"] = self.db["hosts"][host]["status"]["state"]
                    else:
                        info_host["status"] = ""
                    info_host["group"] = []
                    for group in self.db["hosts"][host]["conf"]["groups"]:
                        info_host["group"].append(group)
                    if "date" in self.db["hosts"][host]["status"]:
                        info_host["last_check"] = self.db["hosts"][host]["status"]["date"]
                    else:
                        info_host["last_check"] = 0
                    info_host["warning"] = []
                    info_host["danger"] = []
                    info_host["success"] = []
                    info_host["partial_subscribers"] = False
                    for mod in self.db["hosts"][host]["monitoring"]:
                        if self.db["hosts"][host]["monitoring"][mod]["state"] == "warning":
                            info_host["warning"].append(mod)
                        elif self.db["hosts"][host]["monitoring"][mod]["state"] == "danger":
                            info_host["danger"].append(mod)
                        else:
                            info_host["success"].append(mod)

                        if 'subscribers' in self.db["hosts"][host]["monitoring"][mod] and \
                                        len(self.db["hosts"][host]["monitoring"][mod]["subscribers"].keys()) > 0:
                            info_host["partial_subscribers"] = True
                    info_host['nb_subscribers'] = len(self.db['hosts'][host]['conf']['subscribers'].keys())
                    info_host['subscribers'] = self.db['hosts'][host]['conf']['subscribers']
                    info_host['display_name'] = self.db['hosts'][host]['conf']['display_name']
                    info_host['nb_mod'] = len(self.db['hosts'][host]['monitoring'].keys())
                    res.append(info_host)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_host_informations(self, addr_host):
        """
        Get every informations necessary for the hostpage summary page.
        :param addr_host: the IP address of the host
        :return: informations concerning this host on the form :
          {
            "interventions":list,
            "detected":
              {
                 modname:
                    {
                       key:string,
                       ...
                    }
                 }
             "hostname":string,
             "monitoring":
                {
                   modname:
                      {
                         "date":datetime,
                         "state":string,
                         "value":string
                       }
                },
              "activated_monitoring":{
                    mod_name: bool
                },
             'subparts": [str, ...]
             "custom_infos":string
             "status": string
           }
        """
        self.open_db()
        res = {}
        try:
            res_nmap = json.loads(self.db['hosts'][addr_host]['detected']['nmap'])
            if 'hostname' in res_nmap:
                res['hostname'] = json.loads(self.db['hosts'][addr_host]['detected']['nmap'])['hostname']
            else:
                res['hostname'] = ''
            res['monitoring'] = self.db['hosts'][addr_host]['monitoring']
            res['detected'] = {}
            for mod in self.db['hosts'][addr_host]['detected']:
                res['detected'][mod] = json.loads(self.db['hosts'][addr_host]['detected'][mod])
            res['activated_monitoring'] = {}
            res['subparts'] = {}
            res['subscribers'] = {}
            for mod in self.db['hosts'][addr_host]['conf']['monitoring']:
                res['activated_monitoring'][mod] = self.db['hosts'][addr_host]['conf']['monitoring'][mod]['activated']
                res['subparts'][mod] = self.db['hosts'][addr_host]['conf']['monitoring'][mod]['subparts']
                res['subscribers'][mod] = self.db['hosts'][addr_host]['conf']['monitoring'][mod]['subscribers']
            res['custom_infos'] = self.db['hosts'][addr_host]['conf']['custom_info']
            res['interventions'] = self.db['hosts'][addr_host]['conf']['interventions']
            res['status'] = self.db['hosts'][addr_host]['status']['state']
            res['display_name'] = self.db['hosts'][addr_host]['conf']['display_name']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_nmap_result(self, addr_host):
        """
        Returns the stored nmap result of a host.
        :param addr_host: The IP address of the host
        :return: a dictionary containing the nmap result
        """
        self.open_db()
        res = {}
        try:
            res = json.loads(self.db['hosts'][addr_host]['detected']['nmap'])
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def remove_host(self, args):
        """
        Called from the hostpage
        Removes a host from the database.
        If the host is part of a group, it will be also removed from these groups.
        :param args: A dictionary containing:
        {'addr_host': string}
        """
        addr_host = args['addr_host']
        self.open_db()
        try:
            # removing monitoring entries for this host
            if addr_host in self.db['hosts']:
                del self.db['hosts'][addr_host]
            # removing this host for each group it is registered in
            for group_id in self.db['groups']:
                for host in self.db['groups'][group_id]['hosts']:
                    if addr_host in self.db['groups'][group_id]['hosts']:
                        self.db['groups'][group_id]['hosts'].remove(host)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def save_detection(self, addr_host, name_part, json_res_str):
        """
        Called by a detection module in order to save his detection on the database.
        :param addr_host: the IP adress of the host detected
        :param name_part: the name of the detection_module which have done the detection
        :param json_res_str: a string containing the results of the detection in json
        """
        self.open_db()
        try:
            self.db["hosts"][addr_host]["detected"][name_part] = json_res_str
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def update_nmap_attribute(self, args):
        """
        Called from the hostpage.
        Updatdes an attribute detected by nmap for a given host.
        :param args: a dictionary containing :
        {
          'attribute': string,
          'addr_host': string,
          'new_value': string
        }
        """
        self.open_db()
        try:
            nmap_detection = json.loads(self.db['hosts'][args['addr_host']]['detected']['nmap'])
            nmap_detection[args['attribute']] = args['new_value']
            self.db['hosts'][args['addr_host']]['detected']['nmap'] = json.dumps(nmap_detection)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def change_display_name(self, args):
        """
        Called from the hostpage.
        Updatdes the name to display of a given host.
        :param args: a dictionary containing :
        {
          'addr_host': string,
          'display_name': string,
        }
        """
        self.open_db()
        try:
            self.db['hosts'][args['addr_host']]['conf']['display_name'] = args['display_name']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def update_custom_informations(self, args):
        """
        Called from the hostpage.
        Updates the custom informations stored on the host's configuration
        :param args: a structure containing the values :
        {
            'addr_host': string,
            'txt': string
        }
        """
        addr_host = args['addr_host']
        txt = args['txt']
        self.open_db()
        try:
            self.db["hosts"][addr_host]["conf"]["custom_info"] = txt
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def add_intervention(self, args):
        """
        Called from the hostpage.
        Add a new intervention, stored on the host's configuration
        :param args: a structure containing the values :
        {
            'addr_host': string,   => the IP adress of the host
            'user' : string,       => the username responsible of the intervention
            'date' : string,       => the intervention's date
            'details' : string     => a string explaining the intervetion
        }
        """
        addr_host = args['addr_host']
        intervention = {}
        intervention['username'] = args['user']
        intervention['date'] = args['date']
        intervention['details'] = args['details']
        self.open_db()
        try:
            self.db["hosts"][addr_host]["conf"]["interventions"].append(intervention)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def set_monitoring_activation(self, addr_host):
        """
        Called from the hostpage. Put the monitoring of a host on a idlling state, or activates back its monitoring.
        :param addr_host: the IP addres of the host to idle the monitoring or resume it
        """
        res_instr = {}
        res_instr['add'] = []
        self.open_db()
        try:
            if self.db['hosts'][addr_host]['status']['state'] != "idling":
                # the monitoring was activated, we put it on idle state
                for mod in self.db['hosts'][addr_host]['conf']['monitoring']:
                    if self.db['hosts'][addr_host]['conf']['monitoring'][mod]['activated']:
                        self.db['hosts'][addr_host]['conf']['monitoring'][mod]['activated'] = False
                        self.db['hosts'][addr_host]['conf']['idling']['modules'].append(mod)
                self.db['hosts'][addr_host]['conf']['idling']['status'] = self.db['hosts'][addr_host]['status']['state']
                self.db['hosts'][addr_host]['status']['state'] = "idling"
                res_instr['rem'] = None
            else:
                # the monitoring was in an indle state, we activate it again
                for mod in self.db['hosts'][addr_host]['conf']['idling']['modules']:
                    self.db['hosts'][addr_host]['conf']['monitoring'][mod]['activated'] = True
                    add_instr = {}
                    add_instr['addr'] = addr_host
                    add_instr['mod_name'] = mod
                    add_instr['time'] = datetime.now()
                    add_instr['freq'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod]['check_frequency']
                    res_instr['add'].append(add_instr)
                self.db['hosts'][addr_host]['conf']['idling']['modules'] = []
                self.db['hosts'][addr_host]['status']['state'] = self.db['hosts'][addr_host]['conf']['idling']['status']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res_instr

    def config_mod_activation(self, args):
        """
        Activates or desactivates monitoring modules for a given host
        :param args: a dictionary containing :
        {
            'addr_host': string,
            'activated':{
                mod_name: bool
            }
        }
        :return: a dictionary containing the instructions to send to the process monitoring in the form:
        {
            'add': [{'addr': str, 'mod_name': str, 'time': datetime, 'freq': int}, ...]
            'rem': [{'addr': str, 'mod_name': str}, ... ]
        }
        """
        addr_host = args['addr_host']
        dict_instr = {}
        dict_instr['add'] = []
        dict_instr['rem'] = []
        self.open_db()
        try:
            for mod_name in args['activated']:
                # first case : the monitoring module have never been activated for this host
                if mod_name not in self.db["hosts"][addr_host]["conf"]["monitoring"]:
                    # we copy the global configuration of this module on the host configuration
                    self.db["hosts"][addr_host]["conf"]["monitoring"][mod_name] = self.db['global_conf'][mod_name]
                    self.db["hosts"][addr_host]["conf"]["monitoring"][mod_name]['activated'] = args['activated'][mod_name]
                    # we now add an instruction on the monitoring list
                    add_instr = {}
                    add_instr['addr'] = addr_host
                    add_instr['mod_name'] = mod_name
                    add_instr['time'] = datetime.now()
                    add_instr['freq'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['check_frequency']
                    dict_instr['add'].append(add_instr)
                elif not self.db["hosts"][addr_host]["conf"]["monitoring"][mod_name]["activated"] == \
                        args['activated'][mod_name]:
                    # second case, the configuration module have changed
                    if args['activated'][mod_name]:
                        # if it is now activated, we have to add it to the monitoring list
                        self.db["hosts"][addr_host]["conf"]["monitoring"][mod_name]["activated"] = True
                        add_instr = {}
                        add_instr['addr'] = addr_host
                        add_instr['mod_name'] = mod_name
                        add_instr['time'] = datetime.now()
                        add_instr['freq'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['check_frequency']
                        dict_instr['add'].append(add_instr)
                    else:
                        # if not, we have to remove it from the monitoring list
                        self.db["hosts"][addr_host]["conf"]["monitoring"][mod_name]["activated"] = False
                        rem_instr = {}
                        rem_instr['addr'] = args['addr_host']
                        rem_instr['mod_name'] = mod_name
                        dict_instr['rem'].append(rem_instr)
                        # and from the monitoring data structure
                        if mod_name in self.db['hosts'][addr_host]['monitoring']:
                            del self.db['hosts'][addr_host]['monitoring'][mod_name]
                        state = 'success'
                        for mod_in_monitoring in self.db['hosts'][addr_host]['monitoring']:
                            if self.db['hosts'][addr_host]['monitoring'][mod_in_monitoring]['state'] == 'danger':
                                state = 'danger'
                            elif self.db['hosts'][addr_host]['monitoring'][mod_in_monitoring]['state'] == 'warning' \
                                    and not state == 'danger':
                                state = 'warning'
                        self.db['hosts'][addr_host]['status']['state'] = state

        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return dict_instr
    """
    @staticmethod
    def get_lastest_instance_number(mod_list, mod_name):
        instances_keys = []
        for instance_id in mod_list:
            if re.sub('[0-9]+$', '', instance_id) == mod_name:
                str_id = re.sub('^.*_', '', instance_id)
                instances_keys.append(int(str_id))
        if instances_keys == []:
            return 0
        else:
            return max(instances_keys)
    """

    def get_conf_mod(self, args):
        """
        Called from the hostpage.
        Returns a structure containing informations about the settings of a monitoring module for a given host.
        :param args: a list containing the arguments :
        {'addr_host': string, 'mod_name':string]
        :return: a structure containing :
        {
            'unit' : string,       // the unit of the value
            'minor_limit' : int,
            'major_limit' : int,
            'freq' : int        // in seconds
        }
        """
        addr_host = args['addr_host']
        mod_name = args['mod_name']
        self.open_db()
        res = {}
        try:
            res['unit'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['unit']
            res['minor_limit'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['minor_limit']
            res['major_limit'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['major_limit']
            res['freq'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['check_frequency']
            for period in ['min', 'hour', 'day', 'week', 'month', 'year']:
                res['nb_' + period] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['nb_' + period]
            if 'param' in self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]:
                res['param'] = self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['param']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def set_conf_mod(self, args):
        """
        Called from the hostpage. Save a new configuration for a monitoring module and for a specific host.
        :param args: a structure containing the values :
        {
            'addr_host': string,
            'mod_name': string,
            'freq' : int,
            'minor_limit' : int,
            'major_limit' : int
        }
        """
        addr_host = args['addr_host']
        mod_name = args['mod_name']
        self.open_db()
        try:
            self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['minor_limit'] = args['minor_limit']
            self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['major_limit'] = args['major_limit']
            self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['check_frequency'] = args['freq']
            for period in ['min', 'hour', 'day', 'week', 'month', 'year']:
                if args['nb_' + period] == '':
                    self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['nb_' + period] = None
                else:
                    self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['nb_' + period] = args['nb_' + period]
            self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['param'] = args['param']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def set_prio_conn(self, args):
        """
        Called from the hostpage. Save the priorities of connections modules for a host.
        :param args: A dictionary containing:
        {'addr_host' : string,
         'priorities':{
           mod_name: string
         }
        }
        """
        addr_host = args['addr_host']
        self.open_db()
        try:
            for mod in args['priorities']:
                if mod not in self.db['hosts'][addr_host]['conf']['connections']:
                    # if the module have never been configurated for this host
                    self.db['hosts'][addr_host]['conf']['connections'][mod] = {}
                self.db['hosts'][addr_host]['conf']['connections'][mod]['priority'] = args['priorities'][mod]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def set_conf_conn(self, args):
        """
        Called from the hostpage. Save the connection configuration of a host.
        :param args: A dictionary containting:
          {'addr_host': string,
             'modname': string,
             'current_config': val}
        """
        addr_host = args['addr_host']
        modname = args['modname']
        self.open_db()
        try:
            for param in args['current_config'][modname]:
                if modname not in self.db['hosts'][addr_host]['conf']['connections']:
                    # this is the first time this connection is configured for this host
                    self.db['hosts'][addr_host]['conf']['connections'][modname] = {}
                self.db['hosts'][addr_host]['conf']['connections'][modname][param] = \
                    args['current_config'][modname][param]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_conf_conn(self, addr_host):
        """
        Get the configured connections of a host by priority.
        :param addr_host: The IP address of the host.
        :return: A list, ordored by crescent priority, containing:
        [{'conn_mod_name': string, 'priority': int, 'args': {arg1: val, ...}}]
        """
        res = []
        self.open_db()
        try:
            for conn in self.db['hosts'][addr_host]['conf']['connections']:
                if self.db['hosts'][addr_host]['conf']['connections'][conn]['priority'] > 0:
                    dict_conn = {}
                    dict_conn['conn_mod_name'] = conn
                    dict_conn['priority'] = self.db['hosts'][addr_host]['conf']['connections'][conn]['priority']
                    dict_conn['args'] = self.db['hosts'][addr_host]['conf']['connections'][conn]
                    if res == []:
                        res.append(dict_conn)
                    else:
                        pos = 0
                        while pos < len(res) and res[pos]['priority'] < dict_conn['priority']:
                            pos += 1
                        res.insert(pos, dict_conn)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def add_check(self, addr_host, mod_name, val):
        """
        Called by the module loader after calling a monitoring module.
        Add a new check of a host from a specific module.
        Add the previous entry of monitoring to the archive and call update_stats to update the statistics.
        :param addr_host: the IP adress of the host checked
        :param mod_name: the name of the monitoring_module which have done the check
        :param val: the value observed
        :return: a dictionary that will be used to launch the notification modules in the form :
        {
          notif_mod: [{'username': string, 'moni_mod': string, 'title': string, 'msg': string}, ...],
          ...
        }
        """
        dict_notif = {}
        self.open_db()
        try:
            if mod_name not in self.db['hosts'][addr_host]["monitoring"]:
                # creating the monitoring structure for this module if not exists
                self.db['hosts'][addr_host]["monitoring"][mod_name] = {}
                self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_warning'] = 0
                self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_danger'] = 0
                self.db['hosts'][addr_host]['stats'][mod_name] = {}
                self.db['hosts'][addr_host]['archive'][mod_name] = {}

            dict_new_val = {"date": str(datetime.now()),
                            "value": val,
                            "nb_warning": self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_warning'],
                            "nb_danger": self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_danger']}

            if isinstance(val, type({})):
                # if dictionary
                for key in val:
                    dict_new_val = self.set_new_val(val[key],
                                     dict_new_val,
                                     self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['minor_limit'],
                                     self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['major_limit'])
            else:
                dict_new_val = self.set_new_val(val,
                                           dict_new_val,
                                           self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['minor_limit'],
                                           self.db['hosts'][addr_host]['conf']['monitoring'][mod_name]['major_limit'])

            self.db['hosts'][addr_host]['monitoring'][mod_name] = dict_new_val
            #  updating the global state of the host
            self.db['hosts'][addr_host]['status']['date'] = dict_new_val['date']

            if 'state' not in self.db['hosts'][addr_host]['status']:
                self.db['hosts'][addr_host]['status']['state'] = dict_new_val['state']
            else:
                # now updating the global state of the host (most recent check and worst state for all modules)
                state = 'success'
                for mod_name_stored in self.db['hosts'][addr_host]['monitoring']:
                    if self.db['hosts'][addr_host]['monitoring'][mod_name_stored]['state'] == 'danger':
                        state = 'danger'
                    elif self.db['hosts'][addr_host]['monitoring'][mod_name_stored]['state'] == 'warning' \
                            and not state == 'danger':
                        state = 'warning'
                self.db['hosts'][addr_host]['status']['state'] = state

            #  create a notification structure if the state is not a success
            if dict_new_val['state'] != 'success':
                dict_notif = self.create_notif_structure(addr_host, mod_name, dict_new_val['state'], val)
                # incrementation of the consecutive fails nunmber for this module
                self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_'+dict_new_val['state']] += 1
            else:
                # we set the consecutive fail number to 0
                self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_warning'] = 0
                self.db['hosts'][addr_host]["monitoring"][mod_name]['nb_danger'] = 0

            #  updating and saving statistics
            self.db['hosts'][addr_host]['stats'][mod_name] = \
                self.update_stats(self.db['hosts'][addr_host]['stats'][mod_name], val)
            self.db['hosts'][addr_host]['archive'][mod_name] = \
                self.update_archive(self.db['hosts'][addr_host]['archive'][mod_name],
                                    dict_new_val,
                                    self.db['hosts'][addr_host]['conf']['monitoring'][mod_name])
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return dict_notif

    @staticmethod
    def set_new_val(val, dict_val, minor_limit, major_limit):
        new_status = ""
        if isinstance(val, type(True)):
            #  if boolean
            if minor_limit and not val:
                # if warning set for fail and fail occures
                new_status = 'warning'
            elif major_limit and not val:
                # if danger set for fail and fail occures
                new_status = 'danger'
            else:
                new_status = 'success'
        else:
            #  if numerical value
            if val >= int(minor_limit):
                # if value is above waning limit
                new_status = 'warning'
                if val >= int(major_limit):
                    # if value is above danger limit
                    new_status = 'danger'
            else:
                new_status = 'success'
        if 'state' in dict_val:
            #  multiple values because the value returned by the moni mod is a dictionary
            if new_status == 'danger':
                dict_val['state'] = 'danger'
            elif new_status == 'warning' and dict_val['state'] != 'danger':
                dict_val['state'] = 'warning'
            #  is success in other cases, so success is in dict_val or have been already overwrited
        else:
            #  first value of the dictionary, or unique value
            dict_val['state'] = new_status
        return dict_val

    @staticmethod
    def update_stats(stats, val):
        """
        Updates calulated statistics once a new value is received.
        :param stats: a dictionary taken from the database and corresponding to the statistics stored
        :param val: the new value
        :return: the statistics dictionary updated
        """
        if isinstance(val, type(True)):
            if val:
                val = 1
            else:
                val = 0
        if isinstance(val, type({})):
            tab_iter = val.keys()
        else:
            tab_iter = ['']
            val = {'': val}
        for key in tab_iter:
            if key not in stats:
                stats[key] = {}
                stats[key]['nb_check'] = 0
                stats[key]['delta'] = 0
                stats[key]['mean'] = 0
                stats[key]['M2'] = 0
                stats[key]['lin_reg'] = 0
                stats[key]['min'] = maxint
                stats[key]['max'] = 0
                stats[key]['total'] = 0
            stats[key]['nb_check'] += 1
            stats[key]['total'] += val[key]
            if stats[key]['min'] > val[key]:
                stats[key]['min'] = val[key]
            if stats[key]['max'] < val[key]:
                stats[key]['max'] = val[key]
            #  Compute linear regression
            stats[key]['lin_reg'] += val[key] * stats[key]['nb_check']
            #  Compute variance
            stats[key]['delta'] = val[key] - stats[key]['mean']
            stats[key]['mean'] += stats[key]['delta'] / stats[key]['nb_check']
            stats[key]['M2'] += stats[key]['delta'] * (val[key] - stats[key]['mean'])
        return stats

    @staticmethod
    def update_archive(dict_archive, dict_check, conf_mod):
        if dict_archive == {}:
            for period in ['min', 'hour', 'day', 'week', 'month', 'year']:
                dict_archive[period] = []
                dict_archive[period].append(dict_check)
        else:
            date_last_check = datetime.strptime(dict_check['date'],'%Y-%m-%d %H:%M:%S.%f')
            for period in ['min', 'hour', 'day', 'week', 'month', 'year']:
                date_period_str = dict_archive[period][len(dict_archive[period]) - 1]['date']
                date_period = datetime.strptime(date_period_str,'%Y-%m-%d %H:%M:%S.%f')
                time_diff = date_last_check - date_period
                if period == 'min' and time_diff.seconds >= 60:
                    dict_archive[period].append(dict_check)
                if period == 'hour' and time_diff.seconds >= 3600:
                    dict_archive[period].append(dict_check)
                if period == 'day' and time_diff.days >= 1:
                    dict_archive[period].append(dict_check)
                if period == 'week' and time_diff.days >= 7:
                    dict_archive[period].append(dict_check)
                if period == 'month' and time_diff.days >= 30:
                    dict_archive[period].append(dict_check)
                if period == 'year' and time_diff.days >= 365:
                    dict_archive[period].append(dict_check)
                if conf_mod['nb_' + period] is not None:
                    size_arch = len(dict_archive[period])
                    while size_arch > conf_mod['nb_' + period]:
                        dict_archive[period].pop(0)
                        size_arch -= 1
        return dict_archive

    def create_notif_structure(self, addr_host, moni_mod, status, val):
        """
        Creates and returns a data structure that will be used by the notification modules.
        :param addr_host: the IP address of the host
        :param moni_mod: the name of the monitoring module which have been launched
        :param status: the status of the last check
        :return: data that will be used to launch the notification modules in the form :
        {
          notif_mod: [{'user': dict, 'moni_mod': string, 'title': string, 'msg': string}, ...],
          ...
        }
        """
        host_display_name = self.db['hosts'][addr_host]['conf']['display_name']
        consecutive_fails = self.db['hosts'][addr_host]["monitoring"][moni_mod]['nb_' + status]

        common_title = "[Mum] " + status + " for " + host_display_name + " (" + addr_host + ") with " + moni_mod
        common_msg = "Mum reported a " + status + " for " + host_display_name + " (" + addr_host + ") with " + moni_mod+ " module for the " + str(consecutive_fails) + " consecutive time "
        common_msg += "The last value checked for this part is: " + str(val) + ".\n"

        # Lists to avoid to send the same message to the same user twice if registered on the same host from different ways
        users_from_group = []
        users_from_host = []

        #msg += "The following users will be notified: "
        #msg += str(self.db['hosts'][addr_host]['conf']['subscribers'])
        dict_notif = {}
        notif_type = ""
        if status == 'warning':
            notif_type = 'minor'
        else:
            notif_type = 'major'

        # 1) group notifications
        for group in self.db['groups']:
            # creates a message for all subscribers in a group which the host is member
            if addr_host in self.db['groups'][group]['hosts']:
                for username in self.db['groups'][group]['subscribers']:
                    for notif_mod in self.db['groups'][group]['subscribers'][username][notif_type]:
                        param_notif = self.db['groups'][group]['subscribers'][username][notif_type][notif_mod]
                        if param_notif['activated'] and \
                                        param_notif['priority'] == consecutive_fails:
                            users_from_group.append(username)
                            title = common_title + " on group " + group
                            msg = common_msg + "This machine is a member of the group " + group + " which you are registered."
                            if notif_mod not in dict_notif:
                                dict_notif[notif_mod] = []
                            dict_notif[notif_mod].append({'user': self.db['users'][username]['settings'],
                                                          'moni_mod': moni_mod,
                                                          'title': title,
                                                          'msg': msg})

        # 2) host notifications
        for username in self.db['hosts'][addr_host]['conf']['subscribers']:
            # creates a message for all subscribers in the host
            for notif_mod in self.db['hosts'][addr_host]['conf']['subscribers'][username][notif_type]:
                #{'priority': int, 'activated': bool}
                param_notif = self.db['hosts'][addr_host]['conf']['subscribers'][username][notif_type][notif_mod]
                if param_notif['activated'] and param_notif['priority'] == consecutive_fails and username not in users_from_group:
                    users_from_host.append(username)
                    msg = common_msg
                    if users_from_group != []:
                        msg += "The following users will also be notified : " + str(users_from_group)
                    if notif_mod not in dict_notif:
                        dict_notif[notif_mod] = []
                    dict_notif[notif_mod].append({'user': self.db['users'][username]['settings'],
                                                  'moni_mod': moni_mod,
                                                  'title': "[Mum] " + status + " on " + addr_host,
                                                  'msg': msg})

        # 3) service notifications
        for username in self.db['hosts'][addr_host]['conf']['monitoring'][moni_mod]['subscribers']:
            for notif_mod in self.db['hosts'][addr_host]['conf']['monitoring'][moni_mod]['subscribers'][username][notif_type]:
                param_notif = self.db['hosts'][addr_host]['conf']['monitoring'][moni_mod]['subscribers'][username][notif_type][notif_mod]
                if param_notif['activated'] and param_notif['priority'] == consecutive_fails and username not in users_from_group\
                    and username not in users_from_host:
                    title = common_title
                    msg = common_msg
                    if users_from_group != []:
                        msg += "The following users will also be notified (group notifications): " + str(users_from_group) + "\n"
                    if users_from_host != []:
                        msg += "The following users will also be notified (host notifications): " + str(users_from_host) + "\n"
                    if notif_mod not in dict_notif:
                        dict_notif[notif_mod] = []
                    dict_notif[notif_mod].append({'user': self.db['users'][username]['settings'],
                                                  'moni_mod': moni_mod,
                                                  'title': "[Mum] " + status + " on " + addr_host,
                                                  'msg': msg})
        return dict_notif

    def get_stats(self, args):
        """
        Returns the archived data and row statistics concerning a host.
        :param args: a dictionary containing:
        {
            'addr_host': str
        }
        :return: a dictionary containing:
        {
            'stats': {mod_name: {...}, ...}
            'archive': {mod_name: {...}, ...}
        }
        """
        res = {}
        self.open_db()
        try:
            res['stats'] = self.db['hosts'][args['addr_host']]['stats']
            res['archive'] = self.db['hosts'][args['addr_host']]['archive']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def add_host_list_to_group(self, args):
        """
        Called from the groups page.
        Add given hosts to a group. If the group doesn't exists on the database, it will be created.
        :param args: a dictionary containing :
        {
            'host_list': list<string>,  # a list of IP adresses
            'group'; string       # the group name
        }
        """
        group = args['group']
        host_list = args['host_list']
        self.open_db()
        try:
            # if the group is not registered, we create it
            if group not in self.db['groups']:
                self.db['groups'][group] = {}
                self.db['groups'][group]['hosts'] = []
                self.db['groups'][group]['subscribers'] = {}
            for host in host_list:
                if group not in self.db['hosts'][host]['conf']['groups']:
                    self.db['groups'][group]['hosts'].append(host)
                    self.db['hosts'][host]['conf']['groups'].append(group)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def remove_host_list_to_group(self, args):
        """
        Called from the groups page.
        Remove given hosts to a group. If the group is empty afterwards, il will be also deleted.
        :param args: a dictionary containing :
        {
            'host_list': list<string>,  # a list of IP adresses
            'group'; string       # the group name
        }
        """
        group = args['group']
        host_list = args['host_list']
        self.open_db()
        try:
            for host in host_list:
                self.db['hosts'][host]['conf']['groups'].remove(group)
                self.db['groups'][group]['hosts'].remove(host)
            # deletion of the group if empty
            if len(self.db['groups'][group]['hosts']) == 0:
                del self.db['groups'][group]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def update_subscription_to_group(self, args):
        """
        Called from the notifications page. Updates the subscription to a following user from a host.
        :param args:
        { 'group': string,
          'subscription':
            username:{
             {'minor':
                { notif_mod: priority },
                ...
             },{
             'major':
                { notif_mod: priority },
                ...
             }
            }
        }
        :return:
        """
        group = args['group']
        self.open_db()
        try:
            self.db['groups'][group]['subscribers'] = args['subscription']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_groups(self, args):
        """
        Get all the groups informations
        :param args: None (necessary for dynamic call via websocket)
        :return: a dicionnary containing :
        { grp_name: { 'hosts': [host1, ...], 'subscribers':
           { usr: { 'minor': int, 'major': int }, ...
        } }
        """
        res = {}
        self.open_db()
        try:
            res = self.db['groups']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_group_subscribers(self, args):
        """
        Called from the notification page.
        :param args: A dictionary containing :
        { 'group': string }
        :return: a dictionary containing :
        { username:
          {'minor':
             { notif_mod: priority },
             ...
           },{
           'major':
             { notif_mod: priority },
                ...
        }}}
        """
        group = args['group']
        res = []
        self.open_db()
        try:
            res = self.db['groups'][group]['subscribers']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def create_user(self, args):
        """
        Called from the users page.
        Create a basic empty structure on the database for a new user.
        :param args: a dictionary containing :
        { 'username': string }
        """
        username = args['username']
        self.open_db()
        try:
            self.db['users'][username] = {}
            self.db['users'][username]['preferences'] = {}
            self.db['users'][username]['preferences']['minor_notifications'] = {}
            self.db['users'][username]['preferences']['major_notifications'] = {}
            self.db['users'][username]['account'] = {}
            self.db['users'][username]['settings'] = {}
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_users(self, args):
        """
        Called from the user managment page.
        :param args: None (necessary for dynamic call via websocket)
        :return: A list containing the users registered.
        """
        res = []
        self.open_db()
        try:
            for user in self.db['users']:
                res.append(user)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_user_settings(self, args):
        """
        Called from the profile page.
        :param args: a dictionary containing:
        { 'username': string }
        :return: a dictionary containing:
        {
          'settings':{
            'email': string,
            ...
        }}
        """
        res = {}
        res['settings'] = {}
        self.open_db()
        try:
            res['settings'] = self.db['users'][args['username']]['settings']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def update_user_settings(self, args):
        """
        Called from the profile page. Replaces the current settings of a given user.
        :param args: a dictionary containing:
        {
            'username': string,
            'settings':{
                'email': string,
                ...
            }
        }
        :return:
        """
        self.open_db()
        try:
            self.db['users'][args['username']]['settings'] = args['settings']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_user_subscriptions(self, username):
        """
        Called from profile and users page.
        Get every subscriptions of a given user (host or group), by notifications modules, including the number of
        itterations necessary to get the notification.
        :param username: the user name
        :return: a dictionary containing :
        {
            'hosts':{
              addr_host:{
               'major':{
                  notif_mod:{
                     'priority': int,
                     'activated': bool
                  }, ...
               }
               'minor':{...}
              }, ...
            },
            'groups':{ ... }
        }
        """
        res = {}
        res['hosts'] = {}
        res['groups'] = {}
        self.open_db()
        try:
            for addr_host in self.db['hosts']:
                if username in self.db['hosts'][addr_host]['conf']['subscribers']:
                    res['hosts'][addr_host] = self.db['hosts'][addr_host]['conf']['subscribers'][username]
            for group in self.db['groups']:
                if username in self.db['groups'][group]['subscribers']:
                    res['groups'][group] = self.db['groups'][group]['subscribers'][username]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def remove_user(self, args):
        """
        Called from the users page. Removes a user from the database. If the user is registered to a host or a group,
        it is also deleted from these lists.
        :param args: a dictionary containing :
        { 'username': string }
        """
        username = args['username']
        self.open_db()
        try:
            # deletion of the user
            del self.db['users'][username]
            # unregistering user from hosts
            for host in self.db['hosts']:
                if username in self.db['hosts'][host]['conf']['subscribers']:
                    del self.db['hosts'][host]['conf']['subscribers'][username]
            # unregistering user form groups
            for group in self.db['groups']:
                if username in self.db['groups'][group]['subscribers']:
                    del self.db['groups'][group]['subscribers'][username]
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def update_subscription_to_host(self, args):
        """
        Called from the notifications page. Updates the subscription to a following user from a host.
        :param args:
        { 'addr_host' :str,
          'group': string,
          'subscription':
            username:{
             {'minor':
                { notif_mod: priority },
                ...
             },{
             'major':
                { notif_mod: priority },
                ...
             }
            }
        }
        :return:
        """
        addr_host = args['addr_host']
        self.open_db()
        try:
            self.db['hosts'][addr_host]['conf']['subscribers'] = args['subscription']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_host_subscribers(self, args):
        """
        Called from the notifications page. Get informations about the subscribers to a host.
        :param args: A dictionary containing :
        { 'addr_host': string }
        :return: a dictionary containing :
        { username:
          {'minor':
             { notif_mod: priority },
             ...
           ,
           'major':
             { notif_mod: priority },
                ...
        }}}
        """
        addr_host = args['addr_host']
        res = {}
        self.open_db()
        try:
            res = self.db['hosts'][addr_host]['conf']['subscribers']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def get_service_subscribers(self, args):
        """
        Called from the notifications page. Get informations about the subscribers to a host.
        :param args: A dictionary containing :
        { 'addr_host': string,
          'service': string
        }
        :return: a dictionary containing :
        { username:
          {'minor':
             { notif_mod: priority },
             ...
           ,
           'major':
             { notif_mod: priority },
                ...
        }}}
        """
        addr_host = args['addr_host']
        service = args['service']
        res = {}
        self.open_db()
        try:
            res = self.db['hosts'][addr_host]['conf']['monitoring'][service]['subscribers']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res

    def update_subscription_to_service(self, args):
        """
        Called from the notifications page. Updates the subscription to a following user from a host.
        :param args:
        { 'addr_host' : str,
          'service': string,
          'subscription':
            username:{
             {'minor':
                { notif_mod: priority },
                ...
             },{
             'major':
                { notif_mod: priority },
                ...
             }
            }
        }
        :return:
        """
        addr_host = args['addr_host']
        service = args['service']
        self.open_db()
        try:
            self.db['hosts'][addr_host]['conf']['monitoring'][service]['subscribers'] = args['subscription']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def store_task(self, task_id):
        """
        Stores a new task into the database
        :param task_id: the id of the task
        """
        self.open_db()
        try:
            self.db['tasks'].append(task_id)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def remove_task(self, task_id):
        """
        Removes a new task into the database
        :param task_id: the id of the task
        """
        self.open_db()
        try:
            self.db['tasks'].remove(task_id)
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()

    def get_task_list(self):
        """
        :return: The list of current tasks
        """
        res = []
        self.open_db()
        try:
            res = self.db['tasks']
        except Exception:
            self.logger.error(traceback.format_exc())
        finally:
            self.close_db()
            return res