# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

from xml.dom import minidom
import pexpect
import json
import logging
from random import choice
from string import letters


class nmap_detection:
    def __init__(self, opt, db, ws, list_mod_conn, dict_mod_monitoring, command, hnfe):
        self.opt = opt
        self.db = db
        self.ws = ws
        self.filename = "".join(choice(letters) for i in range(10)) + ".xml"  # generating a filename for xml output
        self.scanned_ip = []
        self.list_mod_conn = list_mod_conn
        self.dict_mod_monitoring = dict_mod_monitoring
        self.HostNotFoundException = hnfe
        self.command = command
        self.logger = logging.getLogger("mum_log")

    #  function for splitting the different ranges of the IP adress
    #  launch the nmap detection of each ip under this range
    #  NB : it is possible to launch nmap directly on the range, but it becomes difficult to determine the state
    #       of the scan. Moreover,if the ip range is very big, the execution will take more time and each result
    #       will be avaliable only a the end of the scan of the range.
    def check_ip_range(self, ip_range):
        #  separation of the 4 bytes
        range_byte_1 = ip_range.split('.')[0]
        range_byte_2 = ip_range.split('.')[1]
        range_byte_3 = ip_range.split('.')[2]
        range_byte_4 = ip_range.split('.')[3]

        #  separation of eventual ranges
        split_byte_1 = range_byte_1.split('-')
        split_byte_2 = range_byte_2.split('-')
        split_byte_3 = range_byte_3.split('-')
        split_byte_4 = range_byte_4.split('-')

        #  if no range is indicated, we create one of same value
        if len(split_byte_1) == 1:
            split_byte_1.append(split_byte_1[0])
        #  checking that numbers are ordored correctly
        #  and that there values are under 255
        split_byte_1 = self.check_order_and_under_255(split_byte_1)

        #  same for the second byte
        if len(split_byte_2) == 1:
            split_byte_2.append(split_byte_2[0])
        split_byte_2 = self.check_order_and_under_255(split_byte_2)

        #  same for the third byte
        if len(split_byte_3) == 1:
            split_byte_3.append(split_byte_3[0])
        split_byte_3 = self.check_order_and_under_255(split_byte_3)

        #  same for the fourth byte
        if len(split_byte_4) == 1:
            split_byte_4.append(split_byte_4[0])
        split_byte_4 = self.check_order_and_under_255(split_byte_4)

        #  it's possible here to check the size of the range

        #  for each range in increasing order, beginning by the last byte
        for byte_1 in range(int(split_byte_1[0]), int(split_byte_1[1]) + 1):
            for byte_2 in range(int(split_byte_2[0]), int(split_byte_2[1]) + 1):
                for byte_3 in range(int(split_byte_3[0]), int(split_byte_3[1]) + 1):
                    for byte_4 in range(int(split_byte_4[0]), int(split_byte_4[1]) + 1):
                        try:
                            self.launch_detection(byte_1, byte_2, byte_3, byte_4)
                        except self.HostNotFoundException as hnfe:
                            self.logger.warning(hnfe.__str__())
                            self.ws.send(json.dumps({"ERROR": hnfe.__str__()}))
        #  once finished, returns the list of scanned ip
        return json.dumps(self.scanned_ip)

    #  check that the numbers are ordered increasing
    #  and their values are under 255
    #  if it is not the case, returns the list in increasing order, and/or with values capped at 255
    def check_order_and_under_255(self, tab_val):
        if int(tab_val[0]) > 255:
            tab_val[0] = '255'
        if int(tab_val[1]) > 255:
            tab_val[1] = '255'
        if int(tab_val[0]) > int(tab_val[1]):
            tmp = tab_val[1]
            tab_val[1] = tab_val[0]
            tab_val[0] = tmp
        return tab_val

    #  launch the nmap detection for the IP adress represented by the 4 bytes in parameters
    def launch_detection(self, byte_1, byte_2, byte_3, byte_4):
        ip = str(byte_1) + '.' + str(byte_2) + '.' + str(byte_3) + '.' + str(byte_4)
        self.ws.send(json.dumps({"CURRENT_STATE_INFO": "Scanning ip : " + ip}))
        try:
            child = pexpect.spawn(self.command + ' ' + self.opt + ' ' + ip + ' -oX ' + self.filename)
            while child.isalive():
                child.expect('Completed', timeout=None)
        except pexpect.EOF:
            self.parse_res(ip)
        except pexpect.TIMEOUT:
            self.logger.error("Timeout on nmap execution")
            self.ws.send(json.dumps({"ERROR": "Timeout on nmap execution"}))
        except pexpect.ExceptionPexpect:
            self.logger.error("nmap command not avaliable on server")
            self.ws.send(json.dumps({"ERROR": "nmap command not avaliable on server"}))

    def launch_detection_with_hostname(self, hostname):
        self.ws.send(json.dumps({"CURRENT_STATE_INFO": "Scanning host : " + hostname}))
        try:
            child = pexpect.spawn(self.command + ' ' + self.opt + ' ' + hostname + ' -oX ' + self.filename)
            while child.isalive():
                child.expect('Completed', timeout=None)
        except pexpect.EOF:
            self.parse_res(hostname)
            return json.dumps(self.scanned_ip)
        except pexpect.TIMEOUT:
            self.logger.error("Timeout on nmap execution")
            self.ws.send(json.dumps({"ERROR": "Timeout on nmap execution"}))
        except pexpect.ExceptionPexpect:
            self.logger.error("nmap command not avaliable on server")
            self.ws.send(json.dumps({"ERROR": "nmap command not avaliable on server"}))

    #  parse the xml result to keep only interesting values
    #  save directly it on the database
    def parse_res(self, ip):
        # opening the xml file with minidom parser
        try:
            root = minidom.parse(self.filename)
        except IOError:
            exception_inst = getattr(self.HostNotFoundException, "HostNotFoundException")(ip)
            raise exception_inst
        pexpect.run("rm -f " + self.filename)
        collection = root.documentElement

        # get every <host> of the collection
        hosts = collection.getElementsByTagName("host")

        # if host cannot have been detected, throws an exception
        if hosts == []:
            exception_inst = getattr(self.HostNotFoundException, "HostNotFoundException")(ip)
            raise exception_inst

        # Get the nodes of each <host> and recuperaton of their attributes
        # JSON = dictionary list
        for host in hosts:
            status = host.getElementsByTagName('status')[0]
            address = host.getElementsByTagName('address')[0]

            dict_host = {}
            dict_host['addr'] = address.getAttribute('addr')
            #dict_host['date'] = host.getAttribute('endtime')
            #dict_host['state'] = status.getAttribute('state')
            dict_host['os'] = 'unknown'  # par defaut
            dict_host['hostname'] = ''
            try:
                hostnames_elem = host.getElementsByTagName('hostnames')[0]
                hostnames = hostnames_elem.getElementsByTagName('hostname')
                for hostname in hostnames:
                    dict_host['hostname'] = hostname.getAttribute("name")
            except IndexError:
                pass
            try:
                ports_elem = host.getElementsByTagName('ports')[0]
                ports = ports_elem.getElementsByTagName('port')
                list_dict_port = []
                for port in ports:
                    dict_port = {}
                    state = port.getElementsByTagName('state')[0]
                    service = port.getElementsByTagName('service')[0]
                    if service.hasAttribute("ostype"):
                        dict_host['os'] = service.getAttribute("ostype").lower()
                    if state.getAttribute('state') == 'open':
                        dict_port['portid'] = port.getAttribute('portid')
                        dict_port['portname'] = service.getAttribute('name')
                        list_dict_port.append(dict_port)
                dict_host['openports'] = list_dict_port
            except IndexError:
                dict_host['openports'] = []
            try:
                dict_host['traceroute'] = []
                trace_elem = host.getElementsByTagName('trace')[0]
                for hop in trace_elem.getElementsByTagName('hop'):
                    dict_host['traceroute'].append(hop.getAttribute("ipaddr"))
                if dict_host['addr'] in dict_host['traceroute']:
                    dict_host['traceroute'].remove(dict_host['addr'])
            except IndexError:
                dict_host['traceroute'] = []
            #  the host have its IP for ID on the db
            self.db.add_host(dict_host['addr'],
                             json.dumps(dict_host),
                             self.list_mod_conn,
                             self.dict_mod_monitoring)
            self.scanned_ip.append(dict_host['addr'])