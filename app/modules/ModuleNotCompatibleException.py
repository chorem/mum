# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

"""
Raised if the module cannot have perform his treatment on a host (command not found or result unexpected)
"""


class ModuleNotCompatibleException(Exception):
    def __init__(self, mod_name, addr_host):
        self.mod_name = mod_name
        self.addr_host = addr_host

    def __str__(self):
        return "Module '" + self.mod_name + "' not compatible on host " + self.addr_host