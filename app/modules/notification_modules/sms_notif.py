# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import urllib2
import re
import logging

def get_class_name():
    return "SMS"


class SMS:
    def __init__(self, user_data, title, msg, settings):
        self.name = get_class_name()
        self.user_data = user_data # {'email': val, ...}
        self.title = title
        self.msg = msg
        self.settings = settings  # = conf attribute on ModuleLoader class
        self.logger = logging.getLogger("mum_log")

    def get_name(self):
        return self.name

    def notify(self):
        url = re.sub(r'#message#', self.msg, self.user_data['sms_url'])

        req = urllib2.Request(url)
        try:
            urllib2.urlopen(req)
        except IOError, e:
            if hasattr(e,'code'):
                if e.code == 400:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code 400: One of the url parameters is missing.')
                elif e.code == 402:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code 402: Too many SMS have been sent in short time.')
                elif e.code == 403:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code 403: The service is not activated or wrong login/key.')
                elif e.code == 404:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code 404: The SMS URL does not exists.')
                elif e.code == 500:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code 500: Server error. Please try again later.')
                else:
                    self.logger.error('sms_notif ' + self.user_data['sms_url'] + ' code ' + e.code)