# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

import json

class WebSocketContainer:

    def __init__(self, db):
        self.ws_set = set()
        self.db = db

    def add_websocket(self, ws):
        self.ws_set.add(ws)

    def remove_websocket(self, ws):
        self.ws_set.discard(ws)

    def notify_state_change(self):
        for ws in self.ws_set:
            ws.send(json.dumps({"RES_GET_HOSTS": self.db.get_hosts()}))

    def notify_task_change(self):
        for ws in self.ws_set:
            ws.send(json.dumps({"RES_TASK_LIST": self.db.get_task_list()}))

    def notify_module_deactivation(self, msg):
        for ws in self.ws_set:
            ws.send(json.dumps({"DEACTIVATION_NOTIF": msg}))