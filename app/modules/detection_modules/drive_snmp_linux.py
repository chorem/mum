# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux', 'unix']
connection = "snmp_walk"
part = 'Disk'

def run_detection(conn):
    oid_mounted_partitions = "1.3.6.1.4.1.2021.9.1.2"
    mounted_partitions = conn.exec_command(oid_mounted_partitions)
    partition_list = []
    for partition in mounted_partitions:
        partition_list.append(mounted_partitions[partition])
    # removing dupe names
    return list(set(partition_list))