# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux', 'unix']
connection = "ssh"
part = 'Disk'


def run_detection(conn):
    """
    Retourne les informations des partitions systeme sous la forme :
    {"sr0": {"mountpoint": "none", "type": "rom", "name": "sr0", "size": "1024M"}
    """
    cmd = "lsblk -r --output=NAME,SIZE,TYPE,MOUNTPOINT"
    stdout = conn.exec_command(cmd)

    # Output example:
    """
    NAME SIZE TYPE MOUNTPOINT
    sda 111,8G disk
    sda1 55,9G part /
    sda2 7,6G part [SWAP]
    sda3 28G part /home
    sda4 20,4G part /var/local
    sr0 1024M rom
    """

    dict_total = {}
    i = 1
    ignore = True
    for line in stdout.splitlines():
        #  On ignore la premiere ligne qui ne contient pas de valeurs
        if ignore:
            ignore = False
        else:
            dict_drive = {}
            tab_elem = line.split()
            dict_drive["name"] = tab_elem[0]
            dict_drive["size"] = tab_elem[1]
            dict_drive["type"] = tab_elem[2]
            if len(tab_elem) > 3:
                dict_drive["mountpoint"] = tab_elem[3]
            else:
                dict_drive["mountpoint"] = "none"
            #  meilleur nom pour chaque attribut ?
            dict_total[dict_drive["name"]] = dict_drive
            i += 1
    return dict_total