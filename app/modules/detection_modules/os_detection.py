# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'


compatible_os = ['linux', 'unix']
connection = "ssh"

def run_detection(conn):
    dict_total = {}
    cmd = "cat /etc/os-release"
    stdout = conn.exec_command(cmd)

    # Output example:
    """
    PRETTY_NAME="Debian GNU/Linux 7 (wheezy)"
    NAME="Debian GNU/Linux"
    VERSION_ID="7"
    VERSION="7 (wheezy)"
    ID=debian
    ANSI_COLOR="1;31"
    HOME_URL="http://www.debian.org/"
    SUPPORT_URL="http://www.debian.org/support/"
    BUG_REPORT_URL="http://bugs.debian.org/"
    """

    for line in stdout.splitlines():
        tab_elem = line.split("=")
        #  to remove the "" on every fields (if any)
        tab_right = tab_elem[1].split('"')
        if len(tab_right) == 1:
            dict_total[str.lower(tab_elem[0])] = tab_right[0]
        else:
            dict_total[str.lower(tab_elem[0])] = tab_right[1]
    # we get all the output, it is possible to add a filter here...
    return dict_total