# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__copyright__ = "Copyright 2015, Code Lutin"
__license__ = "AGPL"

__author__ = 'aguilbaud'

compatible_os = ['linux', 'unix']
connection = "ssh"


def run_detection(conn):
    cmd = "netstat -tuln"
    stdout = conn.exec_command(cmd)

    # Output example:
    """
    Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat
    tcp        0      0 127.0.0.1:63342         0.0.0.0:*               LISTEN
    tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN
    tcp        0      0 0.0.0.0:53205           0.0.0.0:*               LISTEN
    tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
    [...]
    """

    dict_total = {}
    l_number = 0
    for line in stdout.splitlines():
        # we ignore the first 2 lines which contains no information
        if l_number < 2:
            l_number += 1
        else:
            fields = line.split()
            if fields[0] not in dict_total:
                dict_total[fields[0]] = []
            """
            ip_fields = fields[3].split(':') # x.x.x.x:port if IPv4, :::port if IPv6
            port_number = ip_fields[len(ip_fields) - 1]
            dict_total[fields[0]].append(port_number)
            """
            dict_total[fields[0]].append(fields[3])
    return dict_total