%define _topdir	 		/home/aguilbaud/rpmbuild
%define name			mum
%define release			1
%define version 		0.1

%define DEST_APP		/usr/lib/mum
%define DEST_SCRIPTS		/usr/lib/mum/scripts
%define DEST_CONF		/etc
%define DEST_VAR		/var/lib/mum
%define DEST_LOG		/var/log/mum
%define DEST_BIN		/usr/bin

Summary: 		Machines Under Monitoring
License: 		GPL
Name: 			%{name}
Version: 		%{version}
Release: 		%{release}
Source: 		%{name}-%{version}.tar.gz
#Prefix: 		/usr/lib
Group: 			System/Tools
Requires:		python, nmap

%description
Simple monitoring through a web interface using Python.
Detection of the distant hosts with nmap, then 
autoconfiguration from this detection.

%prep
%setup -q

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{DEST_SCRIPTS}
mkdir -p %{buildroot}/%{DEST_CONF}/mum
mkdir -p %{buildroot}/%{DEST_CONF}/soduers.d
mkdir -p %{buildroot}/%{DEST_VAR}/keys
mkdir -p %{buildroot}/%{DEST_LOG}
mkdir -p %{buildroot}/%{DEST_BIN}

scripts/mum-install-bower.sh bower.json %{buildroot}/%{DEST_APP}
rm -rf %{buildroot}/%{DEST_APP}/bower_components/jquery

#cd %{_topdir}
cp -R app %{buildroot}/%{DEST_APP}
cp -R bower_components %{buildroot}/%{DEST_APP}
cp -R static %{buildroot}/%{DEST_APP}
cp -R views %{buildroot}/%{DEST_APP}
cp -R scripts %{buildroot}/%{DEST_APP}
cp -R conf %{buildroot}/%{DEST_APP}
cp conf/mum.conf %{buildroot}/%{DEST_CONF}/mum
cp conf/mum.sudoers.d %{buildroot}/%{DEST_CONF}/sudoers.d/mum
#cp $(mum_daemon) $(DEST_INIT_D)
cp README %{buildroot}/%{DEST_APP}
cp requirements.txt %{buildroot}/%{DEST_APP}
ln -sr %{buildroot}/%{DEST_SCRIPTS}/mum.sh %{buildroot}/%{DEST_BIN}

find %{buildroot}/%{DEST_APP}/app -type f -iname "*.pyc" -exec rm -f {} \;
rm -rf %{buildroot}/%{DEST_APP}/bower_components/jquery

%files
%defattr(-,root,root)
%{DEST_APP}/app
%{DEST_APP}/bower_components
%{DEST_APP}/static
%{DEST_APP}/views
%{DEST_APP}/README
%{DEST_APP}/requirements.txt
%{DEST_SCRIPTS}
%{DEST_BIN}/mum.sh
%attr(0755,mum,mum) %{DEST_VAR}
%attr(0755,mum,mum) %{DEST_LOG}
%config(noreplace) %{DEST_CONF}/mum.conf
%attr(0440,root,root) %config(noreplace) /etc/sudoers.d/mum

%pre
/usr/sbin/useradd --system --user-group mum

%post
if [ -d /lib/systemd/system ]; then
    ln -s /usr/lib/mum/scripts/mum.service /lib/systemd/system/mum.service
elif [ -d /etc/init.d ]; then
    ln -s /usr/lib/mum/scripts/mum-daemon.sh /etc/init.d/mum
fi
/usr/lib/mum/scripts/mum-install-venv.sh

%postun
if [ -L /etc/init.d/mum ]; then
    rm /etc/init.d/mum
fi
if [ -L /lib/systemd/system/mum.service ]; then
    rm /lib/systemd/system/mum.service
fi


%changelog
* Thu May 21 2015 Alexis Guilbaud <aguilbaud@codelutin.com> - 0.1-1
- Initial release
