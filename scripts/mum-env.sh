#!/bin/sh

if [ -z "$MUM_ENV" ]; then

SCRIPT_DIR=$(dirname $(readlink -m $0))

USAGE="usage $(basename $0) [options] [--conf config_file|config_dir]"
MUM_PY=app/mum.py

# looking for --conf argument to find configuration file/dir
nb=0
for arg in "$@"; do
    nb=$(($nb+1))
    case "$arg" in
	--conf=*)
            CONF=${arg#*=}
	    break
            ;;
        --conf*)  
            nb=$(($nb+1))
            CONF=${!nb}
	    break
            ;;
    esac
done

if [ -z "$CONF" ]; then
    if [ -d "/etc/mum" ];then
	# in production 
	CONF=/etc/mum
    elif [ -d "$SCRIPT_DIR/../conf" ];then
	# in development
	CONF=$SCRIPT_DIR/../conf
    fi
fi

CONF=$(readlink -m $CONF)

if [ -d "$CONF" ]; then
    CONF=$CONF/*.conf
fi

if [ -n "$CONF" ]; then
    MUM_DIR=$(grep app_location $CONF |cut -f2 -d=|tail -1)
    VENV_DIR=$(grep venv_location $CONF |cut -f2 -d=|tail -1)
    LOG_DIR=$(dirname "$(grep log_location $CONF |cut -f2 -d=|tail -1)")
fi

if [ ! -f "$MUM_DIR/$MUM_PY" ]; then
    if [ -f "$SCRIPT_DIR/../$MUM_PY" ]; then
        MUM_DIR=$(readlink -m "$SCRIPT_DIR/..")
    elif [ -f /usr/lib/mum/$MUM_PY ]; then
        MUM_DIR=/usr/lib/mum
    else
        echo "Can't find mum installation directory, you must call mum with config argument"
        echo "$USAGE"
        exit 1
    fi
fi

if [ -z "$VENV_DIR" ]; then
    VENV_DIR=$MUM_DIR/venv
fi

if [ -z "$LOG_DIR" ]; then
    if [ -d /var/log/mum ]; then
        LOG_DIR=/var/log/mum
    else
        LOG_DIR=/tmp/mum
    fi
fi

VENV_EXEC=$(which virtualenv)
PYTHON_EXEC=$(which python2)
if [ ! -x "$PYTHON_EXEC" ]; then
    PYTHON_EXEC=$(which python)
fi

if [ ! -x "$PYTHON_EXEC" -o ! -x "$VENV_EXEC" -o -z "$($PYTHON_EXEC -V 2>&1 |grep 'Python 2.')" ]; then
    echo "Can't find python2 or virtualenv, please install them first"
    exit 1
fi

export CONF
export MUM_DIR=$(readlink -m $MUM_DIR)
export APP_PYTHON_DIR=$(readlink -m $MUM_DIR/app)
export APP_VIEWS_DIR=$(readlink -m $MUM_DIR/views)
export APP_STATIC_DIR=$(readlink -m $MUM_DIR/static)
export VENV_DIR=$(readlink -m $VENV_DIR)
export LOG_DIR=$(readlink -m $LOG_DIR)
export MUM_PY=$(readlink -m $MUM_DIR/$MUM_PY)
export PYTHON_EXEC=$(readlink -m $PYTHON_EXEC)
export VENV_EXEC=$(readlink -m $VENV_EXEC)

mkdir -p $LOG_DIR
mkdir -p $(dirname $VENV_DIR)

export MUM_ENV=true

fi
