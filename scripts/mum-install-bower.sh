#!/bin/bash

BOWER_CONF=$(readlink -m $1)
TARGET_DIR=$(readlink -m $2)

if [ -d $MUM_DIR/bower_components ]; then
    ln -s $MUM_DIR/bower_components $TARGET_DIR
else
    cd $TARGET_DIR
    npm install --prefix=$TARGET_DIR bower
    node_modules/bower/bin/bower --quiet --allow-root install $BOWER_CONF
    rm -fr node_modules
fi
