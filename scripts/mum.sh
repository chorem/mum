#!/bin/sh

SCRIPT_DIR=$(dirname $(readlink -m $0))

. $SCRIPT_DIR/mum-env.sh

if [ ! -d "$VENV_DIR" ]; then
    echo "Preparing the virtual environment for the first launch (this may take several minutes)"
    echo "Log files will be saved on $LOG_DIR"
    $SCRIPT_DIR/mum-install-venv.sh 2> $LOG_DIR/mum.install.err | tee $LOG_DIR/mum.install.log | grep "Mum - "
fi
exec $SCRIPT_DIR/mum-in-venv.sh python $MUM_PY $*
