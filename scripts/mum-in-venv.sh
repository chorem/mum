#!/bin/bash

SCRIPT_DIR=$(dirname $(readlink -m $0))

. $SCRIPT_DIR/mum-env.sh

cmd="$1"
shift

. $VENV_DIR/bin/activate
export PYTHONPATH=$PWD/lib:$PYTHONPATH

cd $VENV_DIR
exec "$cmd" "$@"
