#!/bin/bash

SCRIPT_DIR=$(dirname $(readlink -m $0))

. $SCRIPT_DIR/mum-env.sh

#
# Create python env
#

echo "Mum - Installing python environment ..."

$VENV_DIR/bin/python --version >/dev/null 2>&1 || $VENV_EXEC --python=$PYTHON_EXEC $VENV_DIR
$SCRIPT_DIR/mum-in-venv.sh pip install -r $MUM_DIR/requirements.txt

# changing bottle templates in order to make it compatible with angularJS
sed -i 's/{{/[[/g' $VENV_DIR/bin/bottle.py
sed -i 's/}}/]]/g' $VENV_DIR/bin/bottle.py
sed -i 's/{{/[[/g' $VENV_DIR/lib/python*/site-packages/bottle.py
sed -i 's/}}/]]/g' $VENV_DIR/lib/python*/site-packages/bottle.py

#
# creatre bower env if necessary or create simple link
#

echo "Mum - Installing javascript library ..."

$SCRIPT_DIR/mum-install-bower.sh $MUM_DIR/bower.json $VENV_DIR

#
# create all other necessary link
#

echo "Mum - Create application symlink ..."

ln -s $APP_PYTHON_DIR $VENV_DIR
ln -s $APP_VIEWS_DIR  $VENV_DIR
ln -s $APP_STATIC_DIR $VENV_DIR

exit 0
