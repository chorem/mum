
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var mumApp = angular.module('mumApp', ['angularFileUpload', 'angularCharts', 'ngRoute', 'ui.bootstrap', 'toastr', 'ui.select', 'ngSanitize']);

mumApp.factory('DataHosts', function () {
    return {Items: []};
});

mumApp.config(function ($routeProvider) {
    $routeProvider
        .when('/',{
            templateUrl : 'dashboard.html',
            controller : 'dashboardCtrl',
            reloadOnSearch: false
        })
        .when('/dashboard/',{
            templateUrl : 'dashboard.html',
            controller : 'dashboardCtrl',
            reloadOnSearch: false
        })
        .when('/dashboard/:param/',{
            templateUrl : 'dashboard.html',
            controller : 'dashboardCtrl',
            reloadOnSearch: false
        })
        .when('/hostpage/:param/',{
            templateUrl : 'hostpage.html',
            controller : 'hostPageCtrl'
        })
        .when('/notifications/',{
            templateUrl : 'notifications.html',
            controller : 'notificationsCtrl'
        })
        .when('/scan/',{
            templateUrl : 'scan.html',
            controller : 'scanCtrl'
        })
        .when('/scan/:param/',{
            templateUrl : 'scan.html',
            controller : 'scanCtrl'
        })
        .when('/settings/',{
            templateUrl : 'settings.html',
            controller : 'settingsCtrl'
        })
        .when('/signin/',{
            templateUrl : 'signin.html'
            //controller : 'mainController'
        })
        .when('/stats/',{
            templateUrl : 'stats.html',
            controller : 'statsCtrl'
        })
        .when('/users/',{
            templateUrl : 'users.html',
            controller : 'usersCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});

mumApp.config(function($locationProvider){
    $locationProvider.html5Mode(false);
});

mumApp.$inject = ['$scope', '$filter'];

// https://github.com/angular-ui/angular-ui-OLDREPO/blob/master/modules/filters/unique/unique.js (MIT licence)
mumApp.filter('unique', function () {

return function (items, filterOn) {

   if (filterOn === false) {
       return items;
   }

   if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
       var hashCheck = {}, newItems = [];

       var extractValueToCompare = function (item) {
           if (angular.isObject(item) && angular.isString(filterOn)) {
               return item[filterOn];
           } else {
               return item;
           }
       };

       angular.forEach(items, function (item) {
           var valueToCheck, isDuplicate = false;

           for (var i = 0; i < newItems.length; i++) {
               if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                   isDuplicate = true;
                   break;
               }
           }
           if (!isDuplicate) {
               newItems.push(item);
           }

       });
       items = newItems;
   }
   return items;
};
});

mumApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

mumApp.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    extendedTimeOut: 2000,
    timeOut: 15000
  });
});