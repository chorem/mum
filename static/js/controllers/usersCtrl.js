
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('usersCtrl', function ($scope, $rootScope, $route, $timeout) {
    $scope.users = {};

    $scope.selected_user = "";

    $scope.new_username = "";

    $scope.user_subscriptions = {}; /*
                                            {
                                                'hosts':{
                                                  addr_host:{
                                                   'major':{
                                                      notif_mod:{
                                                         'priority': int,
                                                         'activated': bool
                                                      }, ...
                                                   }
                                                   'minor':{...}
                                                  }, ...
                                                },
                                                'groups':{ ... }
                                            }
                                    */

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_users', 'args': null}}));

    // receiving the user list
    $scope.$on("resCall", function (event, args) {
        if(args.func == 'get_users'){
            $timeout(function () {
                $scope.users = args.res;
            }, 0);
        }
        if(args.func == 'create_user' || args.func == 'remove_user'){
            $route.reload();
        }
        if(args.func == 'get_user_subscriptions'){
            $timeout(function () {
                $scope.user_subscriptions = args.res;
            }, 0);
            $scope.get_user_settings();
        }
        if (args.func == 'get_user_settings') {
            $timeout(function () {
                $scope.email = args.res.settings.email;
                $scope.sms_url =  args.res.settings.sms_url;
            }, 0);
        }
        if (args.func == 'update_user_settings') {
            $route.reload();
        }
    });

    $scope.addUser = function () {
        var args = {};
        args['username'] = $scope.new_username;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'create_user', 'args': args}}));
        $route.reload();
    };

    $scope.removeUser = function () {
        var args = {};
        args['username'] = $scope.selected_user;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'remove_user', 'args': args}}));
        $route.reload();
    };

    $scope.get_user_subscriptions = function() {
        if ($scope.selected_user != '') {
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_user_subscriptions',
                                    'args': $scope.selected_user}}));
        }
    };

    $scope.get_class = function (sub_part, sub_type, target_name, notif_mod) {
        var res = "";
        if ($scope.user_subscriptions[sub_part][target_name][sub_type][notif_mod]['activated']) {
            res = "success";
        }
        return res;
    };

    $scope.get_user_settings = function () {
        var args = {};
        if ($scope.selected_user != '') {
            args['username'] = $scope.selected_user;
        }

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_user_settings', 'args': args}}));
    };

    $scope.save_settings = function () {
        var args = {};
        args['username'] = $scope.selected_user;
        args['settings'] = {};
        args['settings']['email'] = $scope.email;
        args['settings']['sms_url'] = $scope.sms_url;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_user_settings', 'args': args}}));
    };

});