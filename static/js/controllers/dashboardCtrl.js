
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('dashboardCtrl', function ($scope, $routeParams, $location, $rootScope, $modal, $timeout, DataHosts) {
    /* Host view functions */

    $scope.param = $routeParams.param;  // parameter in URL, null if none

    /*$scope.sort = {
               sortingOrder : 'id',
               reverse : false
           };*/

    //$scope.gap = 5;

    var first_loading = true;

    $scope.items = DataHosts.Items; /* [
                                         {
                                             "addr":"192.168.74.1",
                                             "name":"www.example.com",
                                             "display_name": str,
                                             "status":val, //"success" or "warning" or "danger" or ""
                                             "group":[  "all", ...],
                                             "last_check":string
                                             "subscribers":{
                                                          "uid":val,
                                                          "priority":val
                                                         }
                                             "warning": [mod_name, ...]
                                             "danger": [mod_name, ...]
                                         },
                                         ...
                                     ]
                                   */

    $scope.$on("hostsUpdate", function (event) {
       $scope.items = DataHosts.Items;
       $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_groups','args': null}}));
       $scope.update_group_obj;
       $scope.update_service_obj;
       if (first_loading){
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_LOADED_MONI_MOD": null}));
            first_loading = false;
       }
       else{
            $scope.update_category_obj;
       }
    });

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'remove_host_list_to_group' || args.func == 'add_host_list_to_group') {
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOSTS": ""}));
        }
        if (args.func == 'get_groups') {
            $timeout(function () {
                $scope.groups = args.res;
            }, 0);
        }
    });

    $scope.host_filter = '';    // input field
    $scope.addr_filter = '';    // input field
    $scope.name_filter = '';    // input field
    $scope.group_filter = '';

    $scope.order_val = 'status';     // select field

    $scope.view = 'host_view';

    $scope.fullscreen = false;

    if (Object.keys($location.search()).length == 0) {
        // if no status filter in parameters, show all status
        $scope.status_filter = ["success", "warning", "danger", "idling", ""];
    }
    else {
        // multiple params should be separated by ','

        for (var key in $location.search()){

            if ( key == 'status_filter' ){
                $scope.status_filter = [];
                status_tab = $location.search()[key].split(',');
                for (var i = 0; i<status_tab.length; i++) {
                    $scope.status_filter.push(status_tab[i]);
                }
            }
            else if ( key == 'fullscreen'){
                var fullscreen_enabled = $location.search()[key] == 'true';
                $scope.fullscreen = fullscreen_enabled;
            }
            else{
                $scope[key] = $location.search()[key];
            }
        }
    }

    $scope.update_url = function () {
        var url = "";

        url += "view=" + $scope.view + "&";

        url += "fullscreen=" + $scope.fullscreen + "&";

        url += "host_filter=" + $scope.host_filter + "&";

        url += "addr_filter=" + $scope.addr_filter + "&";

        url += "name_filter=" + $scope.name_filter + "&";

        url += "group_filter=" + $scope.group_filter + "&";

        var nb_attr = $scope.status_filter.length;
        var i = 0;
        url += "status_filter="
        while ( i<nb_attr - 1 ) {
            url += $scope.status_filter[i] + ',';
            i++;
        }
        url += $scope.status_filter[i];

        $location.search(url);
    };

    $scope.set_fullscreen = function(fullscreen_enabled){
        $rootScope.$broadcast("set_fullscreen", fullscreen_enabled);
        $scope.fullscreen = fullscreen_enabled;
        $scope.update_url();
    };

    $scope.status = '';

    $scope.grp = "all";

    $scope.filtering_status = function (item) {
        return($scope.status_filter.lastIndexOf(item.status) >= 0);
    };

    $scope.allGroups = function () {
       var res = []
       for(var i = 0; i<$scope.items.length; i++){
           for(var j = 0; j<$scope.items[i].group.length; j++){
               res.push($scope.items[i].group[j]);
           }
       }
       return res;
    };

    $scope.type_btn_sub = function (nb_sub){
        var type = "";
        if(nb_sub == 0){
            type = "default";
        }
        else{
            type = "primary";
        }
        return type;
    }

    $scope.rem_host_to_group = function(addr_host, grp_name){
        if(grp_name != 'all'){
            var args = {};
            args['host_list'] = [addr_host];
            args['group'] = grp_name;
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'remove_host_list_to_group','args': args}}));
        }
        else{
            $rootScope.$broadcast("popup_warning", "Hosts must be part of 'all' group.")
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOSTS": ""}));
        }
    };

    $scope.add_host_to_group = function(addr_host, grp_name){
        if(grp_name != 'all'){
            var args = {};
            args['host_list'] = [addr_host];
            args['group'] = grp_name;
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'add_host_list_to_group','args': args}}));
        }
    };


    $scope.open_modal_notif = function (addr_host, group_name) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_notif_label.html',
            controller: 'ModalNotifInstanceCtrl',
            size: 'lg',
            resolve: {
                notif_args: function () {
                    return {'addr_host' : addr_host,
                            'group_name': group_name,
                            'service': null};
                }
            }
        });
    };

    /* Group view functions */
    $scope.groups = {};/*{ grp_name: { 'hosts': [host1, ...], 'subscribers':
                           { usr: { 'minor': int, 'major': int }, ...
                         } }
                        */


    $scope.group_obj = {};  /* {group_name:{"hosts":[addr1, ...],"status":"success"}, ...}  */

    $scope.get_group_sub_num = function(grp_name){
        if(grp_name != 'all'){
            res = {};
            res.nb_sub = Object.keys($scope.groups[grp_name].subscribers).length;
            if (res.nb_sub == 0){
                res.btn_type = 'default';
            }
            else{
                res.btn_type = 'primary';
            }
        }
        else{
            res.nb_sub = null;
            res.btn_type = 'default'
        }
        return res;
    };

    $scope.update_group_obj = function (){
        var allGroups = $scope.allGroups();
        for (var i=0, l=allGroups.length ; i<l ; i++){
            var current_group_name = allGroups[i];
            $scope.group_obj[current_group_name] = {};
            $scope.group_obj[current_group_name].total = 0;
            $scope.group_obj[current_group_name].success = 0;
            $scope.group_obj[current_group_name].warning = 0;
            $scope.group_obj[current_group_name].danger = 0;
            $scope.group_obj[current_group_name].idling = 0;
            $scope.group_obj[current_group_name].status = 'success';
        }
        for (var pos_host=0, l=$scope.items.length ; pos_host < l ; pos_host++){
            for (var pos_grp=0, l_grp=$scope.items[pos_host].group.length ; pos_grp<l_grp ; pos_grp++){
                var current_group_name = $scope.items[pos_host].group[pos_grp];
                $scope.group_obj[current_group_name].total ++;
                if ($scope.items[pos_host].status == 'danger'){
                    $scope.group_obj[current_group_name].danger ++;
                    $scope.group_obj[current_group_name].status = 'danger';
                }
                else if ($scope.items[pos_host].status == 'warning'){
                    $scope.group_obj[current_group_name].warning ++;
                    if($scope.group_obj[current_group_name].status != 'danger'){
                        $scope.group_obj[current_group_name].status = 'warning';
                    }
                }
                else if ($scope.items[pos_host].status == 'success'){
                    $scope.group_obj[current_group_name].success ++;
                }
                else{
                    $scope.group_obj[current_group_name].idling ++;
                }
            }
        }
        $scope.update_url();
    };

    /*** SERVICE VIEW FUNCIONS ***/

    $scope.service_obj = {};  /* {mod_name: {'warning': [addr, ...], 'danger': [addr, ...]}, ...} */

    $scope.is_services_in_alert = function (){
        return Object.keys($scope.service_obj).length > 0;
    };

    $scope.update_service_obj = function (){
        for (var i = 0, l=$scope.items.length; i<l; i++){
            tab_warning = $scope.items[i].warning;
            for (var j = 0, l_warn = tab_warning.length; j<l_warn; j++){
                if(!$scope.service_obj.hasOwnProperty(tab_warning[j])){
                    $scope.service_obj[tab_warning[j]] = {};
                    $scope.service_obj[tab_warning[j]].warning = [];
                    $scope.service_obj[tab_warning[j]].danger = [];
                }
                $scope.service_obj[tab_warning[j]].warning.push($scope.items[i].display_name);
            }

            tab_danger = $scope.items[i].danger;
            for (var j = 0, l_dang = tab_danger.length; j<l_dang; j++){
                if(!$scope.service_obj.hasOwnProperty(tab_danger[j])){
                    $scope.service_obj[tab_danger[j]] = {};
                    $scope.service_obj[tab_danger[j]].warning = [];
                    $scope.service_obj[tab_danger[j]].danger = [];
                }
                $scope.service_obj[tab_danger[j]].danger.push($scope.items[i].display_name);
            }
        }
        $scope.update_url();
    };

    /*** CATEGORY VIEW FUNCIONS ***/

    $scope.category_obj = {};

    $scope.loaded_moni_mod = {};

    $scope.is_category_in_alert = function (){
        return Object.keys($scope.category_obj).length > 0;
    };

    $scope.$on("resGetLoadedMoniMod", function (event, args) {
        $timeout(function () {
            $scope.loaded_moni_mod = args;
            update_category_obj();
        }, 0);
    });

    var update_category_obj = function (){
        for (var i = 0, l=$scope.items.length; i<l; i++){
            tab_warning = $scope.items[i].warning;
            for (var j = 0, l_warn = tab_warning.length; j<l_warn; j++){
                var category = $scope.loaded_moni_mod[tab_warning[j]].block;
                if(!$scope.category_obj.hasOwnProperty(category)){
                    $scope.category_obj[category] = {};
                    $scope.category_obj[category].warning = [];
                    $scope.category_obj[category].danger = [];
                }
                $scope.category_obj[category].warning.push($scope.items[i].display_name + " on " + tab_warning[j]);
            }

            tab_danger = $scope.items[i].danger;
            for (var j = 0, l_dang = tab_danger.length; j<l_dang; j++){
                var category = $scope.loaded_moni_mod[tab_danger[j]].block;
                if(!$scope.category_obj.hasOwnProperty(category)){
                    $scope.category_obj[category] = {};
                    $scope.category_obj[category].warning = [];
                    $scope.category_obj[category].danger = [];
                }
                $scope.category_obj[category].danger.push($scope.items[i].display_name + " on " + tab_danger[j]);
            }
        }
        $scope.update_url();
    };
});