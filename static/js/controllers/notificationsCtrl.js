
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('notificationsCtrl', function ($scope, $rootScope, $modal, $route, $timeout, DataHosts) {
    $scope.hosts = DataHosts.Items; /* [
                                         {
                                             "addr":"192.168.74.1",
                                             "name":"www.example.com",
                                             "status":val, //"success" or "warning" or "danger" or ""
                                             "group":[  "all", ...],
                                             "last_check":val	//UNIX time
                                             "subscribers":{
                                                          "uid":val,
                                                          "priority":val
                                                         }
                                             "warning": [mod_name, ...]
                                             "danger": [mod_name, ...]
                                         },
                                         ...
                                     ]
                                   */

    $scope.groups = {};   /*         { grp_name: { 'hosts': [host1, ...], 'subscribers':
                                        { usr: { 'minor': int, 'major': int }, ...
                                     } }
                           */

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_groups','args': null}}));

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'get_groups') {
            $timeout(function () {
                $scope.groups = args.res;
            }, 0);
        }
    });

    $scope.nb_sub = function(sub_obj){
        return Object.keys(sub_obj).length;
    }
});