
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('scanCtrl', function ($scope, $rootScope, $routeParams, $timeout) {
    // Concerning the scan form

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_SCAN_OPTIONS": null}));

    if ($routeParams.param == null) {
        $scope.ip_range = "";     // la plage d'ip entree dans le champ
    }
    else {
        $scope.ip_range = $routeParams.param;
    }

    $scope.nmap_options = {};

    $scope.selected_option = 'No scan';

    $scope.nmap_options_input = '';

    $scope.$on("resGetScanOptions", function (event, args) {
        $timeout(function () {
            $scope.nmap_options = args;
        }, 0);
    });

    $scope.run_detection = function (){
        var args = {};
        args.ip_range = $scope.ip_range;
        args.nmap_options = $scope.nmap_options_input;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"NMAP_SCAN_DEMAND": args}));
        $scope.ip_range = "";
    };
});