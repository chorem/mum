
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('headCtrl', function ($scope, $rootScope, toastr, $interval, $routeParams, $location, $modal, DataHosts) {

    // init
    //$scope.master = {};

    $scope.items = DataHosts.Items;

    $scope.task_list = [];

    $scope.fullscreen = false;

    $scope.$on ("hostsUpdate", function (event) {
       $scope.items = DataHosts.Items;
    });

    // Concerning WebSocket
    var ssl = "";                         
    if ($location.protocol() === "https") {
       ssl = 's';
    }
    var ws = new WebSocket("ws" + ssl + "://" + $location.host() + ":" + $location.port() + "/websocket");

    ws.onopen = function () {
       var request = JSON.stringify({"GET_HOSTS": ""});
       ws.send(request);
       ws.send(JSON.stringify({"TASK_LIST": null}));
    };

    $scope.max_tasks = 2;

    $scope.format_task_list = function (task_list) {
        res = "";
        var i = 0;
        while (i<$scope.task_list.length && i<$scope.max_tasks) {
            res += task_list[i] + ", ";
            i++;
        }
        if ($scope.task_list.length > $scope.max_tasks) {
            res += "+ " + (task_list.length - $scope.max_tasks) + " other tasks";
        }
        return res;
    }

    // instructions received from other controllers
    // args contains a json formatted string
    $scope.$on("sendViaWs", function (event, args) {
        ws.send(args);
    });

    $scope.$on("popup_warning", function(event, args){
        $scope.pop_warning('Warning', args)
    });

    $scope.$on("set_fullscreen", function(event, args){
        $scope.fullscreen = args;
    });

    ws.onmessage = function (evt) {       // actions effectuees lors de la reception d'un message via la websocket
       var obj = JSON.parse(evt.data);
       for (key in obj) {
           switch (key) {
               case "SUCCESS_MODULE":  // Success of a module execution
                   $rootScope.$broadcast("success", obj[key]);
                   toastr.success(obj[key], "Success on module execution");
                   break;
               case "RES_INFO_HOST":  // Informations concerning one host
                   $rootScope.$broadcast("hostInfos", obj[key]);
                   break;
               case "RES_GET_HOSTS":  // List of hosts under monitoring
                   DataHosts.Items = obj[key];
                   $rootScope.$broadcast("hostsUpdate");
                   $scope.$apply(function(){
                      $scope.items = DataHosts.Items;
                   });
                   break;
               case "RES_REM_HOST":
                   $rootScope.$broadcast("remHost");
                   break;
               case "RES_GET_LOADED_MONI_MOD":
                   $rootScope.$broadcast("resGetLoadedMoniMod", obj[key]);
                   break;
               case "RES_GET_LOADED_CONN_MOD":
                   $rootScope.$broadcast("resGetLoadedConnMod", obj[key]);
                   break;
               case "RES_GET_LOADED_NOTIF_MOD":
                   $rootScope.$broadcast("resGetLoadedNotifMod", obj[key]);
                   break;
               case "RES_GET_SCAN_OPTIONS":
                   $rootScope.$broadcast("resGetScanOptions", obj[key]);
                   break;
               case "RES_CALL_FUNC_DB":  // Get a result after calling a funcion on the db
                   $rootScope.$broadcast("resCall", obj[key]);
                   break;
               case "RES_TEST_CONN":
                   if(obj[key]){ // if success of the connection
                        $scope.pop_success("Connection test", "The connection is well configured.");
                   }
                   else{
                        $scope.pop_danger("Connection test", "The connection could not have been established. Please verify its configuration.");
                   }
                   break;
               case "RES_GET_ALL_MODULES_LIST":
                   $rootScope.$broadcast("resGetAllModulesList", obj[key]);
                   break;
               case "CURRENT_STATE_INFO":
                   $scope.$apply(function(){
                       $scope.state = obj[key];
                   });
                   $rootScope.$broadcast("stateUpdate", obj[key]);
                   toastr.info(obj[key], "Current status is :");
                   /*
                   $scope.$apply(function(){
                       $scope.state = obj[key]
                   });*/
                   break;
               case "BROWSER_NOTIFICATION":
                   params = obj[key].split(',');
                   if(params[0]=='success'){
                       $scope.pop_success("Success on " + params[1], params[2]);
                   }
                   else if(params[0] == 'warning'){
                       $scope.pop_warning("Warning on " + params[1], params[2]);
                   }
                   else if(params[0] == 'danger'){
                       $scope.pop_danger("Danger on "+ params[1], params[2]);
                   }
                   break;
                case "KEYS_LIST":
                    $rootScope.$broadcast("keysList", obj[key]);
                    break;
                case "RES_TASK_LIST":
                    $scope.$apply(function(){
                       $scope.task_list = obj[key];
                    });
                    break;
                case "RES_GET_LOGS":
                    $rootScope.$broadcast("resGetLogs", obj[key]);
                    break;
                case "DEACTIVATION_NOTIF":
                    $scope.pop_warning("A module cannot be activated by Mum", obj[key]);
                    break;
                case "ERROR":
                   toastr.error(obj[key], "Server error");
                   break;
                default:
                   break;
           }
       }
    };

    $scope.pop_success = function (title, msg) {
       toastr.success(msg, title);
    };

    $scope.pop_warning = function (title, msg) {
       toastr.warning(msg, title);
    };

    $scope.pop_danger = function (title, msg) {
       toastr.error(msg, title);
    };

    /*
    * Return a vector with the number of hosts with a status of : total, success, warning, danger, idling
    */
    $scope.stateNumber = function () {
       var res = [0,0,0,0,0];
       for (var i = 0; i<$scope.items.length; i++) {
           res[0] = $scope.items.length;
           if ($scope.items[i].status === "success") {
               res[1]++;
           }
           if ($scope.items[i].status === "warning") {
               res[2]++;
           }
           if ($scope.items[i].status === "danger") {
               res[3]++;
           }
           if ($scope.items[i].status === "idling") {
               res[4]++;
           }
       }
       return res;
    };

    $scope.open_modal_log = function () {
        var modalInstance = $modal.open({
            templateUrl: 'modal_log_label.html',
            controller: 'ModalLogInstanceCtrl',
            size: 'lg'
        });
    };

    $scope.open_modal_mod_list = function () {
        var modalInstance = $modal.open({
            templateUrl: 'modal_modules_list.html',
            controller: 'ModalModulesListCtrl',
            size: 'lg'
        });
    };

});

mumApp.controller('ModalNotifInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout, notif_args) {
    $scope.addr_host = null;
    $scope.grp_name = null;
    $scope.service = null;

    $scope.option_selected = "";   // 'grp' OR 'host' OR 'service'

    $scope.notif_mods = [];

    $scope.subscribers = {};/*   { username:
                                   {'minor': {
                                    { notif_mod: priority },
                                       ...
                                     },
                                     'major': {
                                       { notif_mod: priority },
                                          ...
                                  }}}   */

    $scope.users = {};

    $scope.new_notif = {"priority": null, "activated": true};
    $scope.new_notif_type = "";
    $scope.new_mod_notif = "";
    $scope.new_username = "";

    $scope.get_group_subscribers = function () {
        var args = {'group': $scope.grp_name};
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_group_subscribers', 'args': args}}));
    };

    $scope.get_host_subscribers = function () {
        var args = {'addr_host': $scope.addr_host};
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_host_subscribers', 'args': args}}));
    };

    $scope.get_service_subscribers = function () {
        var args = {'addr_host': $scope.addr_host,
                    'service': $scope.service};
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_service_subscribers', 'args': args}}));
    };

    if(notif_args.addr_host != null && notif_args.service == null){
        // notifications by machine
        $scope.addr_host = notif_args.addr_host;
        $scope.option_selected = "host";
        $scope.get_host_subscribers();
    }
    else if(notif_args.group_name != null){
        // notifications by group
        $scope.grp_name = notif_args.group_name;
        $scope.option_selected = "grp";
        $scope.get_group_subscribers();
    }
    else{
        // notifications by service
        $scope.service = notif_args.service;
        $scope.addr_host = notif_args.addr_host;
        $scope.option_selected = "service";
        $scope.get_service_subscribers();
    }

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'get_host_subscribers' || args.func == 'get_group_subscribers' || args.func == 'get_service_subscribers') {
            $timeout(function () {
                $scope.subscribers = args.res;
            }, 0);
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_LOADED_NOTIF_MOD": ""}));
        }
        else if (args.func == 'get_users') {
            $timeout(function () {
                $scope.users = args.res;
            }, 0);
        }
        else if (args.func == 'update_subscription_to_group' || 'update_subscription_to_host'){
            if($scope.option_selected != 'service'){
                $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOSTS": ""}));
            }
            else{
                $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOST_INFO": $scope.addr_host}));
            }
        }
    });

    $scope.$on("resGetLoadedNotifMod", function (event, args) {
        for (var notif_mod_name in args){
            $scope.notif_mods.push(notif_mod_name);
        }
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_users', 'args': null}}));
    });

    $scope.add_notif = function(){
        if (!$scope.subscribers.hasOwnProperty($scope.new_username)){
            $scope.subscribers[$scope.new_username] = {};
            $scope.subscribers[$scope.new_username].minor = {};
            $scope.subscribers[$scope.new_username].major = {};
        }
        $scope.subscribers[$scope.new_username][$scope.new_notif_type][$scope.new_mod_notif] = $scope.new_notif;
        $scope.new_notif = {"priority": null, "activated": true};
        $scope.new_notif_type = "";
        $scope.new_mod_notif = "";
        $scope.new_username = "";
        $scope.save();
    };

    $scope.remove_notif = function(username, notif_type, mod_notif_name){
        delete $scope.subscribers[username][notif_type][mod_notif_name];
        if (Object.keys($scope.subscribers[username].minor).length == 0 &&
            Object.keys($scope.subscribers[username].major).length == 0){
            delete $scope.subscribers[username];
        }
        $scope.save();
    };

    $scope.save = function () {
        var args = {};
        args['subscription'] = $scope.subscribers;
        if ($scope.option_selected == 'grp') {
            args['group'] = $scope.grp_name;
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_subscription_to_group', 'args': args}}));
        }
        else if($scope.option_selected == 'host'){
            args['addr_host'] = $scope.addr_host;
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_subscription_to_host', 'args': args}}));
        }
        else{
            args['addr_host'] = $scope.addr_host;
            args['service'] = $scope.service;
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_subscription_to_service', 'args': args}}));
        }
    };

    $scope.close = function () {
        $modalInstance.close();
    };
});

mumApp.controller('ModalLogInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout) {
    $scope.log_content = "";
    $scope.nb_lines = 50;

    $scope.update = function(){
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_LOGS": $scope.nb_lines}));
    };

    $scope.$on("resGetLogs", function (event, args) {
        $timeout(function () {
            $scope.log_content = args;
        }, 0);
    });

    $scope.close = function () {
        $modalInstance.close();
    };
});

mumApp.controller('ModalModulesListCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout) {
    $scope.modules_struct = {}  /* {'monitoring': [...],
                                    ...
                                 */

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_ALL_MODULES_LIST": null}));

    $scope.$on("resGetAllModulesList", function (event, args) {
        $timeout(function () {
            $scope.modules_struct = args;
        }, 0);
    });

    $scope.close = function () {
        $modalInstance.close();
    };
});
