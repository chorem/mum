
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('hostPageCtrl', function ($scope, $rootScope, $route, $routeParams, $location, $modal, $timeout) {

    $scope.loaded = false; // indicates if this is the first loading of the page

    $scope.addr_host = $routeParams.param;

    // asks for host informations
    $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOST_INFO": $routeParams.param}));

    $scope.items = {};/*  result of get_host_informations =>
                          {
                            "interventions":list,
                            "detected":
                              {
                                 modname:
                                    {
                                       key:val,
                                       ...
                                    }
                                 }
                             "hostname":val,
                             "display_name": str,
                             "status": string,
                             "monitoring":
                                {
                                   modname:
                                      {
                                         "date":val,
                                         "state":val,
                                         "value":val
                                       }
                                },
                              'activated_monitoring':{
                                    mod_name: bool
                                },
                             "custom_infos":val
                             "compatible_os_list": [os1, os2, ...]
                             "loaded_moni_mod":
                                {
                                  mod_name:
                                    {
                                        'compatible_os': [val1, val2, ...],    => a list containing the compatibles os
                                        'unit': val,                           => the unit type of return ('%', 'bool' or other)
                                        'block': val,                          => the monitoring block of the module
                                        'compatible_conn': [val1, ...]         => a list of connection modules
                                    }
                                }
                              "subparts":{
                                 modname: [...],
                                 ...
                              }
                           }
                        */

    $scope.collapsed = {};

    $scope.show_display_name_input = false;

    $scope.selectedAll = false;

    $scope.blocks = {};  /*
                            block_name:
                              {
                                mod_name:
                                  {
                                    'modname':val,
                                    'activated': bool
                                  }
                              }
                         */

    $scope.selected_mod_part = "";

    $scope.get_service_sub_num = function(service_name){
        res = {};
        res.nb_sub = Object.keys($scope.items.subscribers[service_name]).length;
        if (res.nb_sub == 0){
            res.btn_type = 'default';
        }
        else{
            res.btn_type = 'primary';
        }
        return res;
    };

    $scope.rescan = function () {
        $location.path('/scan/' + $scope.addr_host);
    };

    $scope.update_nmap_attribute = function (attribute, new_value) {
        var args = {};
        args['attribute'] = attribute;
        args['addr_host'] = $scope.addr_host;
        args['new_value'] = new_value;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_nmap_attribute', 'args': args}}));
    };

    $scope.change_display_name = function (){
        var args = {};
        args['addr_host'] = $scope.addr_host;
        args['display_name'] = $scope.items.display_name;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'change_display_name', 'args': args}}));
    };

    $scope.get_unit = function (mod_name) {
        res = '';
        if ($scope.items.loaded_moni_mod[mod_name].unit != 'bool') {
            res = $scope.items.loaded_moni_mod[mod_name].unit;
        }
        return res;
    };

    $scope.get_maxvalue = function (modname) {
        res = Number.MIN_SAFE_INTEGER;
        for (var key in $scope.items.monitoring[modname].value){
            res = Math.max(res, $scope.items.monitoring[modname].value[key]);
        }
        if ($scope.items.loaded_moni_mod[modname].unit == 'bool'){
            // if success for bool value, want to show 'true' and not '1'
            res = (res == 1);
        }
        return res;
    };

    $scope.get_type_of = function (attr) {
        return typeof(attr);
    };

    $scope.get_idle_state = function () {
        res = "";
        if ($scope.items.status == "idling") {
            res = "danger";
        }
        else {
            res = "success";
        }
        return res
    };

    $scope.popover_message = function () {
        msg = "";
        if ($scope.items.status == "idling") {
            msg = "Click to activate again the monitoring for this host.";
        }
        else {
            msg = "Click to suspend the monitoring for this host.";
        }
        return msg
    };

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'update_nmap_attribute' || 
            args.func == 'update_custom_informations' ||
            args.func == 'change_display_name') {
            $route.reload();
        }
    });

    $scope.model = {custom_infos : '',
                    new_os : '',
                    new_hostname : '',
                    compatible_os_list: []};

    $scope.check = function (modname) {
        var args = {};
        args['mod_name'] = modname;
        args['addr_host'] = $scope.addr_host;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CHECK_NOW": args}));
    };

    $scope.set_idle_state = function () {
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"SET_IDLE_STATE": $scope.addr_host}));
    };

    $scope.get_addr_host = function () {
        return($scope.addr_host);
    };

    $scope.launch_detection = function () {
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"LAUNCH_FULL_DETECTION": $scope.addr_host}));
    };

    // receiving the host informations
    $scope.$on("hostInfos", function (event, args) {
        $timeout(function () {
            if (!$scope.loaded) { // we take all the fields of information
                $scope.items = args;
                $scope.model.custom_infos = args.custom_infos;
                $scope.loaded = true;
                $scope.model['compatible_os_list'] = args['compatible_os_list'];
                for (var modname in args.monitoring) {
                    $scope.collapsed[modname] = true;
                }
            }

            else { // we take only monitoring updates
                $scope.items.monitoring = args.monitoring;
                $scope.items.status = args.status;
                $scope.items.subscribers = args.subscribers;
            }

            // Updating struct for activated modules selection in any case
            $scope.blocks = {};
            for (mod in args.loaded_moni_mod) {
                var activated = false;
                var block = args.loaded_moni_mod[mod]['block'];
                if (args.activated_monitoring.hasOwnProperty(mod)) { // if the module is known by the host
                    if (args.activated_monitoring[mod]) { // if the module is activated
                        activated = true;
                    }
                }
                if (!$scope.blocks.hasOwnProperty(block)) {
                    $scope.blocks[block] = [];
                }
                $scope.blocks[block][$scope.blocks[block].length] = {modname: mod, activated: activated};
            }
        }, 0);
    });

    $scope.update_module_activation = function (modname, bool_activation) {
        var args = {};
        args['addr_host'] = $scope.addr_host;
        args['activated'] = {};
        args['activated'][modname] = bool_activation;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"SET_MOD_ACTIVATION": args}));
    };

    // refresh after full detection
    $scope.$on("success", function (event, args) {
        if (args == 'Full detection') {
            $route.reload();
        }
    });

   // monitoring data are updated
    $scope.$on("hostsUpdate", function (event, args) {
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOST_INFO": $scope.addr_host}));
    });

    // save custom informations
    $scope.save_custom_infos = function () {
        var args = {};
        args.addr_host = $scope.addr_host;
        args.txt = $scope.model.custom_infos;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_custom_informations', 'args': args}}));
    };

    // creation of modals
    $scope.open_modal_conf = function (mod_name) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_conf_label.html',
            controller: 'ModalConfInstanceCtrl',
            resolve: {
                conf_args: function () {
                    return {'addr_host' : $scope.addr_host,
                            'mod_name': mod_name,
                            'detected': $scope.items.detected,
                            'subparts': $scope.items.subparts
                           };
                }
            }
        });
    };

    $scope.open_modal_interv = function () {
        var modalInstance = $modal.open({
            templateUrl: 'modal_interv_label.html',
            controller: 'ModalIntervInstanceCtrl',
            resolve: {
                interv_args: function () {
                    return {'addr_host' : $scope.addr_host};
                }
            }
        });
    };

    $scope.open_modal_conn = function () {
        var modalInstance = $modal.open({
            templateUrl: 'modal_conn_label.html',
            controller: 'ModalConnInstanceCtrl',
            resolve: {
                conn_args: function () {
                    return {'addr_host' : $scope.addr_host};
                }
            }
        });
    };

    $scope.open_modal_confirm_delete = function () {
        var modalInstance = $modal.open({
            templateUrl: 'modal_confirm_delete_label.html',
            controller: 'ModalConfirmDeleteInstanceCtrl',
            resolve: {
                confirm_delete_args: function () {
                    return {'addr_host' : $scope.addr_host};
                }
            }
        });
    };

    $scope.open_modal_port_config = function (port_list) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_port_config.html',
            controller: 'ModalPortConfigInstanceCtrl',
            resolve: {
                port_config_args: function () {
                    return {'addr_host' : $scope.addr_host,
                            'port_list': port_list};
                }
            }
        });
    };

    $scope.open_modal_stats = function (addr_host, modname, subpart) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_stats.html',
            controller: 'ModalStatsCtrl',
            size: 'lg',
            resolve: {
                stats_args: function () {
                    return {'addr_host' : addr_host,
                            'mod': modname,
                            'subpart': subpart};
                }
            }
        });
    };

    $scope.open_modal_notif_service = function (addr_host, service) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_notif_label.html',
            controller: 'ModalNotifInstanceCtrl',
            size: 'lg',
            resolve: {
                notif_args: function () {
                    return {'addr_host' : addr_host,
                            'group_name': null,
                            'service': service};
                }
            }
        });
    };
});

// modals controllers

mumApp.controller('ModalConfInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout, conf_args) {
    $scope.conf_args = conf_args; /*  {'addr_host' : val,
                                        'mod_name', val,
                                        "detected":
                                          {
                                             modname:
                                                {
                                                   key:val,
                                                   ...
                                                }
                                         }
                                           "subparts":{
                                              modname: [...],
                                              ...
                                           }
                                       }
                                   */

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_conf_mod', 'args': conf_args}}));

    $scope.items = {};   /*        {
                                       'unit' : val,       // the unit of the value
                                       'minor_limit' : val,
                                       'major_limit' : val,
                                       'freq' : val,
                                       'param': {param_name:
                                                   {'type': str, 'doc': str, 'value': val},
                                                 ...
                                                }
                                   }
                         */

    $scope.param_struct = {};  /*
                                      {param_type:[{"doc":str,"type":"string","value":val,"param_name":str}, ... }]}
                                */

    $scope.subparts_checked = {};

    // init fields
    $scope.freq_days = 0;
    $scope.freq_hours = 0;
    $scope.freq_minutes = 0;

    $scope.minor_limit_percent = 0;
    $scope.major_limit_percent = 0;

    $scope.minor_limit_unit = 0;
    $scope.major_limit_unit = 0;

    $scope.limit_bool = 'minor';

    $scope.arch_min = 0;
    $scope.arch_hour = 0;
    $scope.arch_day = 0;
    $scope.arch_week = 0;
    $scope.arch_month = 0;
    $scope.arch_year = 0;

    // when the actual conf of the module is received
    $scope.$on("resCall", function (event, args) {
        if (args.func == 'get_conf_mod') {
            $timeout(function () {
                $scope.items = args.res;
                $scope.freq_days = Math.floor(args.res.freq / 86400);
                $scope.freq_hours = Math.floor((args.res.freq - $scope.freq_days * 86400) / 3600);
                $scope.freq_minutes = Math.floor((args.res.freq - ($scope.freq_days * 86400) - ($scope.freq_hours * 3600)) / 60)

                if (args.res.unit == 'bool') {
                    if (args.res.major_limit) {
                        $scope.limit_bool = 'major';
                    }
                }
                else if (args.res.unit == '%') {
                    $scope.minor_limit_percent = args.res.minor_limit;
                    $scope.major_limit_percent = args.res.major_limit;
                }
                else {
                    $scope.minor_limit_unit = args.res.minor_limit;
                    $scope.major_limit_unit = args.res.major_limit;
                }
                $scope.arch_min = args.res.nb_min;
                $scope.arch_hour = args.res.nb_hour;
                $scope.arch_day = args.res.nb_day;
                $scope.arch_week = args.res.nb_week;
                $scope.arch_month = args.res.nb_month;
                $scope.arch_year = args.res.nb_year;
                update_param_struct();


                for (var i=0, size=$scope.param_struct.detection.length ; i<size ; i++) {
                    var modname = $scope.param_struct.detection[i].param_name;
                    $scope.subparts_checked[modname] = {}
                    for (var key in $scope.conf_args.detected[modname]) {
                        var subpart_name = $scope.conf_args.detected[modname][key];
                        $scope.subparts_checked[modname][subpart_name] = $scope.conf_args.subparts[modname].hasOwnProperty(key);
                    }
                }
            }, 0);
        }
    });

    $scope.save_subpart = function (modname) {
        var args = {};
        args['addr_host'] = $scope.conf_args.addr_host;
        args['modname'] = modname;
        args['subpart_list'] = [];
        for (key in $scope.subparts_checked[modname]) {
            if ($scope.subparts_checked[modname][key]) {
                args['subpart_list'].push(key);
            }
        }
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'set_subpart', 'args': args}}));
    };

    $scope.checkAll = function (modname, bool) {
        $scope.selectedAll = bool;
        for(key in $scope.subparts_checked[modname]) {
            $scope.subparts_checked[modname][key] = bool;
        }
    };

    var update_param_struct = function (){
        /*
            Formats the custom parameters to have the type as a key. Permits to generate the form with ng-repeat.
        */
        for (var param_name in $scope.items.param){
            var dict_param = $scope.items.param[param_name];
            dict_param.param_name = param_name;
            if (!$scope.param_struct.hasOwnProperty(dict_param.type)){
                $scope.param_struct[dict_param.type] = [];
            }
            $scope.param_struct[dict_param.type].push(dict_param);
        }
    };

    // after validation
    $scope.ok = function () {
        var args = {};
        args.addr_host = $scope.conf_args.addr_host;
        args.mod_name = $scope.conf_args.mod_name;
        args.freq = 86400 * $scope.freq_days + 3600 * $scope.freq_hours + 60 * $scope.freq_minutes;
        if ($scope.items.unit == 'bool') {
            args.minor_limit = ($scope.limit_bool == 'minor');
            args.major_limit = ($scope.limit_bool == 'major');
        }
        else if ($scope.items.unit == '%') {
            args.minor_limit = $scope.minor_limit_percent;
            args.major_limit = $scope.major_limit_percent;
        }
        else {
            args.minor_limit = $scope.minor_limit_unit;
            args.major_limit = $scope.major_limit_unit;
        }
        args.nb_min = $scope.arch_min;
        args.nb_hour = $scope.arch_hour;
        args.nb_day = $scope.arch_day;
        args.nb_week = $scope.arch_week;
        args.nb_month = $scope.arch_month;
        args.nb_year = $scope.arch_year;

        args.param = $scope.items.param;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"SET_CONF_MOD": args}));

        $modalInstance.close();

        $route.reload();
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
});

mumApp.controller('ModalIntervInstanceCtrl', function ($scope, $rootScope, $modalInstance, $filter, $route, interv_args) {
    $scope.interv_args = interv_args; //  {'addr_host' : val}

    $scope.user = '';
    $scope.date = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm');
    $scope.details = '';

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'add_intervention') {
            $modalInstance.close();
            $route.reload();
        }
    });

    $scope.ok = function () {
        var args = {};
        args.addr_host = $scope.interv_args.addr_host;
        args.user = $scope.user;
        args.date = $scope.date;
        args.details = $scope.details;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'add_intervention', 'args': args}}));
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
});

mumApp.controller('ModalConnInstanceCtrl', function ($scope, $rootScope, $modalInstance, $modal, $route, $timeout, conn_args) {
    $scope.conn_args = conn_args; //  {'addr_host' : val}

    $scope.current_config = {};

    $scope.priorities = {};

    $scope.isNotConfigured = function (modname) {
        res = false;
        /*for(param in $scope.current_config[modname]){
            if($scope.current_config[modname][param] == null){
                res = true;
            }
        }*/
        return res;
    };

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_conn_param','args': {'addr_host': $scope.conn_args['addr_host']}}}));

    // when the configuration of the connection is received
    $scope.$on("resCall", function (event, args) {
        if(args.func == 'get_conn_param'){
            $timeout(function () {
                $scope.current_config = args.res;
                for (mod in args.res) {
                    $scope.priorities[mod] = args.res[mod]['priority']
                }
            }, 0);
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_LOADED_CONN_MOD": ""}));
        }
        if(args.func == 'set_prio_conn'){
            $modalInstance.close();
            $route.reload();
        }
    });

    $scope.$on("resGetLoadedConnMod", function (event, args) {
        $timeout(function () {
            $scope.loaded_conn_mods = args;
            for (mod in $scope.loaded_conn_mods) {
                if (!$scope.current_config.hasOwnProperty(mod)) {
                    $scope.current_config[mod] = {};
                    $scope.current_config[mod]['priority'] = 0;
                }
            }
        }, 0);
    });

    $scope.testConn = function (connModName) {
        var args = {};
        args['addr_host'] = $scope.conn_args["addr_host"];
        args['conn_mod_name'] = connModName;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"TEST_CONN": args}));
    };

    $scope.ok = function () {
        var args = {};
        args['addr_host'] = $scope.conn_args.addr_host;
        args['priorities'] = $scope.priorities;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'set_prio_conn','args': args}}));
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.open_modal_conf_conn = function (modname) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_conf_conn_label.html',
            controller: 'ModalConfConnInstanceCtrl',
            size: 'lg',
            resolve: {
                conf_conn_args: function () {
                    return {'addr_host' : $scope.conn_args['addr_host'],
                    'modname': modname,
                    'current_config': $scope.current_config,
                    'loaded_conn_mods': $scope.loaded_conn_mods};
                }
            }
        });
    };
});

mumApp.controller('ModalConfConnInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout, conf_conn_args, FileUploader) {
    $scope.uploader = new FileUploader({
        url: '/upload'
    });

    $scope.conf_conn_args = conf_conn_args; /*  {'addr_host': val,
                                                 'modname': val,
                                                 'current_config': val
                                                 'loaded_conn_mods':
                                                   {
                                                     modname:{param1:type ,param2:type},
                                                     ...
                                                   }
                                                 } */

    $scope.conf_conn = {};

    $scope.port = $scope.conf_conn_args["current_config"][$scope.conf_conn_args.modname]["port"];

    $scope.username = $scope.conf_conn_args["current_config"][$scope.conf_conn_args.modname]["username"];

    $scope.password = $scope.conf_conn_args["current_config"][$scope.conf_conn_args.modname]["password"];

    $scope.private_key = $scope.conf_conn_args["current_config"][$scope.conf_conn_args.modname]["private_key"];

    $scope.keys_list = [];

    $scope.show_form = function (param_name) {
        return $scope.conf_conn_args.loaded_conn_mods[$scope.conf_conn_args.modname].hasOwnProperty(param_name);
    }

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_KEYS_LIST": ""}));

    $scope.$on("keysList", function (event, args) {
        $timeout(function () {
            $scope.keys_list = args;
        }, 0);
    });

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'set_conf_conn') {
            $modalInstance.close();
            $route.reload();
        }
    });

    $scope.ok = function () {
        var args = conf_conn_args;
        args['current_config'][$scope.conf_conn_args.modname] = {}
        args['current_config'][$scope.conf_conn_args.modname]['port'] = $scope.port;
        args['current_config'][$scope.conf_conn_args.modname]['username'] = $scope.username;
        args['current_config'][$scope.conf_conn_args.modname]['password'] = $scope.password;
        args['current_config'][$scope.conf_conn_args.modname]['private_key'] = $scope.private_key;


        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'set_conf_conn','args': args}}));

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
});


mumApp.controller('ModalConfirmDeleteInstanceCtrl', function ($scope, $rootScope, $modalInstance, $location, $timeout, confirm_delete_args) {
    $scope.confirm_delete_args = confirm_delete_args;

    $scope.ok = function () {
        var args = {};
        args['addr_host'] = $scope.confirm_delete_args.addr_host;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"REM_HOST": args}));
    };

    $scope.$on("remHost", function (event, args) {
        $timeout(function () {
            $rootScope.$broadcast("sendViaWs", JSON.stringify({"GET_HOSTS": ""}));
            $location.path('/');
            $modalInstance.close();
        }, 0);
    });

    $scope.cancel = function () {
        $modalInstance.close();
    };
});

mumApp.controller('ModalPortConfigInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout, port_config_args) {
    $scope.port_config_args = port_config_args; /*
                                                    { 'addr_host': str,
                                                      'port_list': [{'portname':str, 'portid':int}, ...]
                                                    }
                                                */

    $scope.new_portname = null;

    $scope.new_portid = null;

    $scope.remove_port = function (port_dict) {
        var i = 0;
        var port_found = false;
        while (i<$scope.port_config_args.port_list.length && !port_found) {
            if ($scope.port_config_args.port_list[i].portname == port_dict.portname &&
               $scope.port_config_args.port_list[i].portid == port_dict.portid) {
                port_found = true;
                $scope.port_config_args.port_list.splice(i, 1);
            }
            i++;
        }
    };

    $scope.add_port = function () {
        var i = 0;
        var port_found = false;
        while (i<$scope.port_config_args.port_list.length && !port_found) {
            if ($scope.port_config_args.port_list[i].portname == $scope.new_portname &&
               $scope.port_config_args.port_list[i].portid == $scope.new_portid) {
                port_found = true;
            }
            i++;
        }
        if (!port_found) {
            // avoiding the same value twice in the list
            var new_dict_port = {};
            new_dict_port['portname'] = $scope.new_portname;
            new_dict_port['portid'] = $scope.new_portid;
            $scope.port_config_args.port_list.push(new_dict_port);
            $scope.new_portname = null;
            $scope.new_portid = null;
        }
    }

    $scope.ok = function () {
        var args = {};
        args['attribute'] = 'openports';
        args['addr_host'] = $scope.port_config_args.addr_host;
        args['new_value'] = $scope.port_config_args.port_list;
        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'update_nmap_attribute', 'args': args}}));
    };

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'update_nmap_attribute') {
            $route.reload();
            $modalInstance.close();
        }
    });

    $scope.cancel = function () {
        $modalInstance.close();
        $route.reload();
    };
});

mumApp.controller('ModalStatsCtrl', function ($scope, $rootScope, $modalInstance, $route, $timeout, stats_args) {
  $scope.selected_host = stats_args.addr_host;
  $scope.selected_mod = stats_args.mod;
  $scope.selected_subpart = stats_args.subpart;
  $scope.selected_period = 'min';

  //data
  $scope.stats = {};
  $scope.archive = {};

  //calculated statistics
  $scope.mean_value = 0;
  $scope.standard_derivation = 0;
  $scope.slope_lr = 0;

  $scope.get_stats = function() {
    if ($scope.selected_host != null && $scope.selected_host != '') {
      var args = {};
      args['addr_host'] = $scope.selected_host;
      $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_stats', 'args': args}}));
    }
  };

  $scope.get_stats();

  $scope.$on("resCall", function (event, args) {
      if (args.func == 'get_stats') {
          $timeout(function () {
              $scope.stats = args.res.stats;
              $scope.archive = args.res.archive;
              $scope.update_stats();
              $scope.refresh_chart_data();
          }, 0);
      }
  });

  $scope.update_stats = function() {
    update_mean();
    update_standard_derivation();
    update_slope_of_linear_regression();
  };

  $scope.refresh_chart_data = function() {
    $scope.config.title = $scope.selected_host;
    $scope.data.data = [];
    var archive_list = $scope.archive[$scope.selected_mod][$scope.selected_period];
    // need to test if this achive have a subpart (= if its value is an object)
    var first_entry = archive_list[0];
    if (typeof(first_entry.value) == "object") {
      // if this archive have subparts
      // will get the archives for the selected subpart
      $scope.data.series = [$scope.selected_mod + ' on ' + $scope.selected_subpart];
      for (var i = 0, l=archive_list.length ; i<l ; i++) {
        var current_arch = archive_list[i];
        if (current_arch.value.hasOwnProperty($scope.selected_subpart)) {
          $scope.data.data.push({'x': ((l-i) - (l-i)*2) + 1,
                                 'y': [current_arch.value[$scope.selected_subpart]],
                                 'tooltip': current_arch.value[$scope.selected_subpart] + " @ " +
                                         current_arch.date.split('.')[0]
                                }
                               )
        }
      }
    }
    else {
      // there is no subpart
      $scope.data.series = [$scope.selected_mod];
      var archive_list = $scope.archive[$scope.selected_mod][$scope.selected_period]
      for (var i = 0, l=archive_list.length ; i<l ; i++) {
        var current_arch = archive_list[i];
        $scope.data.data.push({'x': ((l-i) - (l-i)*2) + 1,
                               'y': [current_arch.value],
                               'tooltip': current_arch.value + " @ " +
                                       current_arch.date.split('.')[0]
                              }
                             )
      }
    }
  };

  $scope.value_is_NaN = function (value) {
    return value !== value;
  };

  var update_mean = function () {
    var total = $scope.stats[$scope.selected_mod][$scope.selected_subpart].total;
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;

    $scope.mean_value = total / nb_check;
    // rounding at 2 decimals
    $scope.mean_value = Math.round($scope.mean_value * 100) / 100;
  };

  var update_standard_derivation = function () {
    var m2 = $scope.stats[$scope.selected_mod][$scope.selected_subpart].M2;
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;
    var variance = m2 / Math.max(1, nb_check + 1);

    $scope.standard_derivation = Math.sqrt(variance);
    // rounding at 2 decimals
    $scope.standard_derivation = Math.round($scope.standard_derivation * 100) / 100;
  };

  var update_slope_of_linear_regression = function () {
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;
    var total = $scope.stats[$scope.selected_mod][$scope.selected_subpart].total;
    var lin_reg = $scope.stats[$scope.selected_mod][$scope.selected_subpart].lin_reg;

    var mX = (nb_check + 1) / 2;
    var mY = total / nb_check;
    var mX2 = (nb_check + 1) * (2 * nb_check + 1) / 6;
    var mXY = lin_reg / nb_check;

    $scope.slope_lr = (mXY - mX * mY) / (mX2 - mX * mX);
    // rounding at 2 decimals
    $scope.slope_lr = Math.round($scope.slope_lr * 100) / 100;
  };

  // angular_charts fields :
  $scope.config = {
    title: '',
    tooltips: true,
    labels: false,
    mouseover: function () {},
    mouseout: function () {},
    click: function () {},
    legend: {
      display: true,
      //could be 'left, right'
      position: 'right'
    },
    waitForHeightAndWidth: true,
    isAnimate: false
  };

  $scope.data = {
    series: [],
    data: []
  };

  $scope.close = function () {
      $modalInstance.close();
      $route.reload();
  };
});