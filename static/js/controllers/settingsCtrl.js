
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('settingsCtrl', function ($scope, $rootScope, $modal, $timeout) {
    $scope.settings = {}; // { mod_name: {'check_frequency': 60, 'minor_limit': True, 'activated': False, 'major_limit': False, 'block': 'software', 'unit': 'bool'}}

    $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_global_settings','args': null}}));

    $scope.$on("resCall", function (event, args) {
        if (args.func == 'get_global_settings') {
            $timeout(function () {
                $scope.settings = args.res;
            }, 0);
        }
    });

    $scope.all_blocks = function () {
        var res = [];
        for (mod in $scope.settings) {
            if (res.indexOf($scope.settings[mod]['block']) < 0) { // if the block is not in the res tab
                res[res.length] = $scope.settings[mod].block;
            }
        }
        return res;
    };

    $scope.all_mod_by_block = function (block) {
        var res = {};
        for (mod in $scope.settings) {
            if ($scope.settings[mod]['block'] == block) {
                res[mod] = $scope.settings[mod];
            }
        }
        return res;
    };

    $scope.open_modal_global_conf = function (mod_name) {
        var modalInstance = $modal.open({
            templateUrl: 'modal_global_conf_label.html',
            controller: 'ModalGlobalConfInstanceCtrl',
            resolve: {
                global_conf_args: function() {
                    return {'conf_mod': $scope.settings[mod_name], 'mod_name': mod_name};
                }
            }
        });
    };
});

mumApp.controller('ModalGlobalConfInstanceCtrl', function ($scope, $rootScope, $modalInstance, $route, global_conf_args) {
    $scope.global_conf_args = global_conf_args; /* { 'mod_name': str,
                                                      'conf_mod': {'check_frequency': 60, 'minor_limit': True, 'activated': False, 'major_limit': False, 'block': 'software', 'unit': 'bool'}}
                                                   }
                                                 */
    // init fields
    $scope.freq_days = Math.floor(global_conf_args.conf_mod.check_frequency / 86400);
    $scope.freq_hours = Math.floor((global_conf_args.conf_mod.check_frequency - $scope.freq_days * 86400) / 3600);
    $scope.freq_minutes = Math.floor((global_conf_args.conf_mod.check_frequency - ($scope.freq_days * 86400) - ($scope.freq_hours * 3600)) / 60)

    $scope.minor_limit_percent = 0;
    $scope.major_limit_percent = 0;

    $scope.minor_limit_unit = 0;
    $scope.major_limit_unit = 0;

    $scope.limit_bool = 'minor';

    $scope.arch_min = global_conf_args.conf_mod.nb_min;
    $scope.arch_hour = global_conf_args.conf_mod.nb_hour;
    $scope.arch_day = global_conf_args.conf_mod.nb_day;
    $scope.arch_week = global_conf_args.conf_mod.nb_week;
    $scope.arch_month = global_conf_args.conf_mod.nb_month;
    $scope.arch_year = global_conf_args.conf_mod.nb_year;

    if ($scope.global_conf_args.conf_mod.unit == 'bool') {
        if ($scope.global_conf_args.conf_mod.major_limit) {
            $scope.limit_bool = 'major';
        }
    }
    else if ($scope.global_conf_args.conf_mod.unit == '%') {
        $scope.minor_limit_percent = $scope.global_conf_args.conf_mod.minor_limit;
        $scope.major_limit_percent = $scope.global_conf_args.conf_mod.major_limit;
    }
    else {
        $scope.minor_limit_unit = $scope.global_conf_args.conf_mod.minor_limit;
        $scope.major_limit_unit = $scope.global_conf_args.conf_mod.major_limit;
    }

    // after validation
    $scope.ok = function () {
        var args = {};
        args.mod_name = $scope.global_conf_args.mod_name;
        args.freq = 86400 * $scope.freq_days + 3600 * $scope.freq_hours + 60 * $scope.freq_minutes;
        if ($scope.global_conf_args.conf_mod.unit == 'bool') {
            args.minor_limit = ($scope.limit_bool == 'minor');
            args.major_limit = ($scope.limit_bool == 'major');
        }
        else if ($scope.global_conf_args.conf_mod.unit == '%') {
            args.minor_limit = $scope.minor_limit_percent;
            args.major_limit = $scope.major_limit_percent;
        }
        else {
            args.minor_limit = $scope.minor_limit_unit;
            args.major_limit = $scope.major_limit_unit;
        }

        args.nb_min = $scope.arch_min;
        args.nb_hour = $scope.arch_hour;
        args.nb_day = $scope.arch_day;
        args.nb_week = $scope.arch_week;
        args.nb_month = $scope.arch_month;
        args.nb_year = $scope.arch_year;

        $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'set_global_settings','args': args}}));

        $modalInstance.close();

        $route.reload();
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
});