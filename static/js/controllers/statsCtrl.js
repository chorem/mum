
/*
Copyright (C) 2015  Code Lutin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

mumApp.controller('statsCtrl', function ($scope, $rootScope, $timeout, DataHosts) {
  $scope.host_list = [];
  for (var i = 0; i < DataHosts.Items.length; i++) {
    $scope.host_list.push(DataHosts.Items[i]['addr'])
  }
  $scope.mod_list = [];
  $scope.subpart_list = [];

  //selection
  $scope.selected_host = '';
  $scope.selected_mod = '';
  $scope.selected_subpart = '';
  $scope.selected_period = '';

  //data
  $scope.stats = {};
  $scope.archive = {};

  //calculated statistics
  $scope.mean_value = 0;
  $scope.standard_derivation = 0;
  $scope.slope_lr = 0;

  $scope.get_stats = function() {
    if ($scope.selected_host != null && $scope.selected_host != '') {
      var args = {};
      args['addr_host'] = $scope.selected_host;
      $rootScope.$broadcast("sendViaWs", JSON.stringify({"CALL_FUNC_DB": {'func': 'get_stats', 'args': args}}));
    }
  };

  $scope.$on("resCall", function (event, args) {
      if (args.func == 'get_stats') {
          $scope.stats = args.res.stats;
          $scope.archive = args.res.archive;
          $scope.mod_list = [];
          for(var mod_name in args.res.archive){
            $scope.mod_list.push(mod_name);
          }
      }
  });

  $scope.update_subpart_list = function () {
    $scope.subpart_list = [];
    for (subpart in $scope.stats[$scope.selected_mod]) {
      $scope.subpart_list.push(subpart);
    }
    $scope.selected_subpart = '';
    if ($scope.subpart_list.length == 1 && $scope.subpart_list[0] == '') {
      $scope.update_stats();
    }
  };

  $scope.update_stats = function() {
    $scope.selected_period = '';
    update_mean();
    update_standard_derivation();
    update_slope_of_linear_regression();
  };

  $scope.refresh_chart_data = function() {
    $scope.config.title = $scope.selected_host;
    $scope.data.data = [];
    var archive_list = $scope.archive[$scope.selected_mod][$scope.selected_period];
    // need to test if this achive have a subpart (= if its value is an object)
    var first_entry = archive_list[0];
    if (typeof(first_entry.value) == "object") {
      // if this archive have subparts
      // will get the archives for the selected subpart
      $scope.data.series = [$scope.selected_mod + ' on ' + $scope.selected_subpart];
      for (var i = 0; i<archive_list.length; i++) {
        var current_arch = archive_list[i];
        if (current_arch.value.hasOwnProperty($scope.selected_subpart)) {
          $scope.data.data.push({'x': i,
                                 'y': [current_arch.value[$scope.selected_subpart]],
                                 'tooltip': current_arch.value[$scope.selected_subpart] + " @ " +
                                         current_arch.date.split('.')[0]
                                }
                               )
        }
      }
    }
    else {
      // there is no subpart
      $scope.data.series = [$scope.selected_mod];
      var archive_list = $scope.archive[$scope.selected_mod][$scope.selected_period]
      for (var i = 0; i < archive_list.length; i++) {
        var current_arch = archive_list[i];
        $scope.data.data.push({'x': i,
                               'y': [current_arch.value],
                               'tooltip': current_arch.value + " @ " +
                                       current_arch.date.split('.')[0]
                              }
                             )
      }
    }
  };

  $scope.value_is_NaN = function (value) {
    return value !== value;
  };

  var update_mean = function () {
    var total = $scope.stats[$scope.selected_mod][$scope.selected_subpart].total;
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;

    $scope.mean_value = total / nb_check;
  };

  var update_standard_derivation = function () {
    var m2 = $scope.stats[$scope.selected_mod][$scope.selected_subpart].M2;
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;
    var variance = m2 / Math.max(1, nb_check + 1);

    $scope.standard_derivation = Math.sqrt(variance);
  };

  var update_slope_of_linear_regression = function () {
    var nb_check = $scope.stats[$scope.selected_mod][$scope.selected_subpart].nb_check;
    var total = $scope.stats[$scope.selected_mod][$scope.selected_subpart].total;
    var lin_reg = $scope.stats[$scope.selected_mod][$scope.selected_subpart].lin_reg;

    var mX = (nb_check + 1) / 2;
    var mY = total / nb_check;
    var mX2 = (nb_check + 1) * (2 * nb_check + 1) / 6;
    var mXY = lin_reg / nb_check;

    $scope.slope_lr = (mXY - mX * mY) / (mX2 - mX * mX);
  };

  // angular_charts fields :
  $scope.config = {
    title: '',
    tooltips: true,
    labels: false,
    mouseover: function () {},
    mouseout: function () {},
    click: function () {},
    legend: {
      display: true,
      //could be 'left, right'
      position: 'right'
    },
    waitForHeightAndWidth: true
  };

  $scope.data = {
    series: [],
    data: []
  };
});